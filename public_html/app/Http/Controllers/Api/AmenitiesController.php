<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AmenitiesController extends Controller
{
    public function addAmenities(){

        $post_array = array(
            "nameEn"=>"Parking",
            "nameAr"=>"Parking",
            "image"=>"image.jpg"
        );


        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://199.241.139.22:9090/amenties',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>json_encode($post_array),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'token: 47c61acd6cec70106fe1e4e2faa688d701245699fe8e472c4064366561af316c',
            'Authorization: Basic R2VuZTpnZW5lQDEyMw=='
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;

    }

    public function getAmenitiesdetailes(){

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'http://199.241.139.22:9090/amenties',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'token: 47c61acd6cec70106fe1e4e2faa688d701245699fe8e472c4064366561af316c',
            'Authorization: Basic R2VuZTpnZW5lQDEyMw=='
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        echo $response;
        
    }

    public function editAmenities_id(){
       
        $post_array = array(
            "amentiesId"=>"6048af5d635cae981776aa50",
            "nameEn"=>"Hospital",
            "nameAr"=>"Parking",
            "image"=>"image.jpg"
        );


        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL =>config('custom.curl_url_Amenties'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>json_encode($post_array),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'token: 47c61acd6cec70106fe1e4e2faa688d701245699fe8e472c4064366561af316c',
            'Authorization: Basic R2VuZTpnZW5lQDEyMw=='
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }
}
