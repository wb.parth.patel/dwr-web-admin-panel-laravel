<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function addevent(){

            $post_array = array(
                "nameEn"=>"Flat100",
                "nameAr"=>"Flat100", 
                "latitude"=>"12.222",
                "longitude"=>"12.333",      
                "image"=>"image.jpg",
                "status"=>1,
                "bussinessId"=> "ghdcgjhdgcgk",
                "startDate"=>"2021-03-10T07:57:46.171+0000",
                "endDate"=> "2021-03-10T07:57:46.171+0000",
                "deviceType"=> "1",
                "publish"=> "2021-03-10T07:57:46.171+0000",
                "userType"=> "1",
                "area"=>"6049b5737dbc1ea8467eafb7,6049b5737dbc1ea8467eafb7,6049b5737dbc1ea8467eafb7"
            );
        
            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => config('custom.curl_url_Events'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>json_encode($post_array),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'token: 47c61acd6cec70106fe1e4e2faa688d701245699fe8e472c4064366561af316c',
                'Authorization: Basic R2VuZTpnZW5lQDEyMw=='
            ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            echo $response;
    }

    public function geteventdetails(){
        
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => config('custom.curl_url_Events'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'token: 47c61acd6cec70106fe1e4e2faa688d701245699fe8e472c4064366561af316c',
            'Authorization: Basic R2VuZTpnZW5lQDEyMw=='
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }
}
