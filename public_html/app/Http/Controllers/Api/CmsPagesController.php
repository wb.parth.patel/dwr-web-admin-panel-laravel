<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CmsPagesController extends Controller
{
    public function addcmspages(){

        $post_array = array(
            "nameEn"=> "Laptop",
            "nameAr"=>"Laptop",
            "metaData"=> "HI",
            "metakeywords"=> "HI",
            "slug"=> "HI",
            "metaDescription"=>"Purchase all items",
            "desriptionEn"=>"sd",
            "desriptionAr"=>"dscds"
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => config('custom.curl_url_Cmspages'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>json_encode($post_array),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'token: 47c61acd6cec70106fe1e4e2faa688d701245699fe8e472c4064366561af316c',
            'Authorization: Basic R2VuZTpnZW5lQDEyMw=='
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }

    public function getcmspagesdetails(){

            $post_array = array(
                "name"=>"Laptop",
                "metaData"=> "HI",
                "metakeywords"=> "HI",
                "slug"=>"HI",
                "description"=> "Purchase all items"
            );
        
            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => config('custom.curl_url_Cmspages'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS =>c,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'token: 47c61acd6cec70106fe1e4e2faa688d701245699fe8e472c4064366561af316c',
                'Authorization: Basic R2VuZTpnZW5lQDEyMw=='
            ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            echo $response;

    }

    public function editcmspages(){

        $post_array = array(
            "cmsId"=>"6034b6e78577498ca5790d3c",
            "nameEn"=> "Laptop",
            "nameAr"=>"Laptop",
            "metaData"=> "HI",
            "metakeywords"=>"HI",
            "slug"=> "HI",
            "metaDescription"=>"Purchase all items",
            "desriptionEn"=>"sd",
            "desriptionAr"=>"dscds",
            "status"=>false
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => config('custom.curl_url_Cmspages'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>json_encode($post_array),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'token: 47c61acd6cec70106fe1e4e2faa688d701245699fe8e472c4064366561af316c',
            'Authorization: Basic R2VuZTpnZW5lQDEyMw=='
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        echo $response;

    }
}
