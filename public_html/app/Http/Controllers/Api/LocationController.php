<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    public function addlocation(){

            $post_array = array(
                "nameEn"=>"C G Road",
                "nameAr"=>"الاسم الاول",
                "catId"=>"604affbb98dfa6db9b762782",
                "country"=>"Kuwait",
                "image"=>"image.jpeg",
                "latitude"=>"12.223",
                "longitude"=>"12.22",
                "website"=>"www.google.com",
                "hours"=>"2021-03-12T05:44:27.339Z"
            );

            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL =>config('custom.curl_url_Location'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>json_encode($post_array),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'token: 47c61acd6cec70106fe1e4e2faa688d701245699fe8e472c4064366561af316c',
                'Authorization: Basic R2VuZTpnZW5lQDEyMw=='
            ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            echo $response;
    }

    public function getlocationdetails(){


        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL =>config('custom.curl_url_Location'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'token: 47c61acd6cec70106fe1e4e2faa688d701245699fe8e472c4064366561af316c',
            'Authorization: Basic R2VuZTpnZW5lQDEyMw=='
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }
}
