<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AreaController extends Controller
{
     public function addarea(){
            
            $post_array = array(
                "nameEn"=>"Ashoka city",
                "nameAr"=>"Ashoka city",
                "country"=>"Ahemdabad"
            );

            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => config('custom.CURL_URL_Area'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>json_encode($post_array),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                // 'token: 6a2e737835708d307fa83b3ed04a2ddb3a95479d5adf946604ce74364f34f554',
                // 'Authorization: Basic R2VuZTpnZW5lQDEyMw=='
            ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            echo $response;

     }

     public  function getareadetails(){

            $post_array = array(
                "name"=> "Ashoka city",
                "city"=> "Mathura",
                "state"=> "Uttar Pradesh",
                "pincode"=> "281004"
            );

            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => config('custom.CURL_URL_Area'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS =>json_encode($post_array),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                // 'token: 190800b9ddb97b1c315f29ae60c4656b341b45a1cbe0a52eaed7a5a9f6ec0231',
                // 'Authorization: Basic R2VuZTpnZW5lQDEyMw=='
            ),
            ));
            
            $response = curl_exec($curl);
            curl_close($curl);
            echo $response;
     }

    public function editarea(){

        $post_array = array(
            "areaId"=>"6034f68ff3e27394184c4791",
            "nameEn"=> "Ashoka city",
            "nameAr"=>"Ashoka city",
            "country"=> "Mathura",
            "status"=>false
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => config('custom.CURL_URL_Area'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>json_encode($post_array),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            // 'token: a6a1fdbaf4af79cc458332dc9cb559905a44c3d92aefa52e48bc832c695d75cf',
            // 'Authorization: Basic R2VuZTpnZW5lQDEyMw=='
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;

    }
}
