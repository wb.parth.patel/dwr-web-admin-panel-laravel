<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function editsetting(){
        
        $post_array = array(
            "appId"=>"6049e8fb991ddcba7c714bba",
            "nameEn"=>"WanBuffer12",
            "nameAr"=>"WanBuffer",
            "addressAr"=>"Kuwait",
            "addressEn"=>"Kuwait",
            "contactAr"=>"1234567890",
            "contactEn"=>"1234567890",
            "emailAr"=>"test@gmail.com",
            "emailEn"=>"test@gmail.com",
            "firebaseKey"=>"esdfghjkj",
            "gMapKey"=>"xcvbn",
            "latitude"=>"1234.456",
            "logoImage"=>"image.jpg",
            "longitude"=>"12.123",
            "mapBoxKey"=>"gfhhcjjkdsc",
            "tageLineAr"=>"Hi, I am on dev machine.",
            "tageLineEn"=>"Hi, I am on dev machine."
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL =>config('custom.curl_url_Setting'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>json_encode($post_array),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'token: 47c61acd6cec70106fe1e4e2faa688d701245699fe8e472c4064366561af316c',
            'Authorization: Basic R2VuZTpnZW5lQDEyMw=='
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;

    }
}
