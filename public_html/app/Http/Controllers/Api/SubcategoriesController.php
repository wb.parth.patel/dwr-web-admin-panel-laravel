<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SubcategoriesController extends Controller
{
    public function addsubcategories(){

        $post_array = array(
            "nameEn"=>"Grocery",
            "nameAr"=>"Grocery",
            "image"=>"image.jpg",
            "description"=>"Purchase all items",
            "categoryId"=>"6033474d55c9d46d5a6f1d1e"
        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => config('custom.curl_url_Subcategory'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>json_encode($post_array),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'token: 5d32006c10ccdbf971721d986612ba23bac868de3314d8047904813cf195d83a',
            'Authorization: Basic R2VuZTpnZW5lQDEyMw=='
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }
    public function getsubcategoriesdetails(){

        
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => config('custom.curl_url_Subcategory'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'token: 5d32006c10ccdbf971721d986612ba23bac868de3314d8047904813cf195d83a',
            'Authorization: Basic R2VuZTpnZW5lQDEyMw=='
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;

    }
    public function editsubcategories(){

        $post_array = array(
            "subCategoryId"=>"6033642ebd65a776ffcb8ded",
            "categoryId"=>"6033474d55c9d46d5a6f1d1e",
            "nameEn"=>"Grocery",
            "nameAr"=>"Grocery",
            "image"=>"image.jpg",
            "description"=>"Purchase all items",
            "status"=>1
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL =>config('custom.curl_url_Subcategory'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>json_encode($post_array),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'token: 5d32006c10ccdbf971721d986612ba23bac868de3314d8047904813cf195d83a',
            'Authorization: Basic R2VuZTpnZW5lQDEyMw=='
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;


    }
    public function getsubcategories_id(){
        $post_array = array(
            "name"=>"Grocery",
            "image"=>"image.jpg",
            "description"=>"Purchase all items"
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL =>config('custom.curl_url_Subcategory'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_POSTFIELDS =>json_encode($post_array),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'token: 5d32006c10ccdbf971721d986612ba23bac868de3314d8047904813cf195d83a',
            'Authorization: Basic R2VuZTpnZW5lQDEyMw=='
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }



}
