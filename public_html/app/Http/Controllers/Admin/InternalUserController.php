<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InternalUserController extends Controller
{
    public function index(){

        return view('backend.users.manage_dwrinternal_user');
    }
}
