<?php

return [

    'curl_url_Area' => env('CURL_URL_Area', 'http://199.241.139.22:9090/area'),
    'CURL_URL_Label' => env('CURL_URL_Label', 'http://199.241.139.22:9090/label'),
    'curl_url_Categories' => env('curl_url_Categories', 'http://199.241.139.22:9090/category'),
    'curl_url_Cmspages' => env('curl_url_Cmspages', 'http://199.241.139.22:9090/cmspage'),
    'curl_url_Offers' => env('curl_url_Offers', 'http://199.241.139.22:9090/offers'), 
    'curl_url_Events' => env('curl_url_Events', 'http://199.241.139.22:9090/events'), 
    'curl_url_ExplorerPages' => env('curl_url_ExplorerPages', 'http://199.241.139.22:9090/explorerPage'), 
    'curl_url_Location' => env('curl_url_Location', 'http://199.241.139.22:9090/location'), 
    'curl_url_Setting' => env('curl_url_Setting', 'http://199.241.139.22:9090/setting'), 
    'curl_url_Amenties' => env('curl_url_Amenties', 'http://199.241.139.22:9090/amenties'), 
    'curl_url_Subcategory' => env('curl_url_Subcategory', 'http://199.241.139.22:9090/subcategory'),
    'curl_url_Ticket' => env('curl_url_Ticket', 'http://199.241.139.22:9090/ticket'), 
];