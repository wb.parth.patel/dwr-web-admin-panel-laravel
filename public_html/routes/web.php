<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::GET('/', function () {
    return redirect('login');
});
Route::get('logout', '\Auth\LoginController@logout');


Route::group(['namespace' => '\App\Http\Controllers'], function () {
    //Dashboard
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
});

Route::group(['namespace' => '\App\Http\Controllers\Admin'], function () {
    Route::GET('myprofile/index', 'MyProfileController@index')->name('myprofile.index');
    Route::GET('managesupportticket', 'SupportTicketsController@index')->name('managesupportticket.index');
    Route::GET('managebusiness', 'BusinessController@index')->name('managebusiness.index');
    Route::GET('manageapplicationuser', 'UsersController@index')->name('manageapplicationuser.index');
    Route::GET('managedwruser', 'InternalUserController@index')->name('managedwruser.index');
    Route::GET('managesettings', 'SettingsController@index')->name('managesettings.index');
    Route::GET('manageexplorersettings', 'ExplorerPagesController@index')->name('manageexplorersettings.index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
