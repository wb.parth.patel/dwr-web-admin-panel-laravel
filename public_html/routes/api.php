<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => '\App\Http\Controllers\Api'], function () {

    Route::post('signup', 'SignupController@create')->name('signup');
    Route::get('login', 'LoginController@getuserdetails')->name('login');
    Route::post('addarea', 'AreaController@addarea');
    Route::get('getareadetails', 'AreaController@getareadetails');
    Route::post('editarea', 'AreaController@editarea');
    Route::post('addlabel', 'LabelController@addlabel');
    Route::get('getlabeldetails', 'LabelController@getlabeldetails');
    Route::post('editlsbel', 'LabelController@editlsbel');
    Route::post('addcategories', 'CategoryController@addcategories');
    Route::get('getcategorisdetails', 'CategoryController@getcategorisdetails');
    Route::post('editcategoris', 'CategoryController@editcategoris');

});