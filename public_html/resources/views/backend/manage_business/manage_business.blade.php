@extends('backends_layouts.master',['title'=>'Business Management'])
@section('content')
<!--Content Area-->
<div class="l-page-header">
     <h2 class="l-page-title"><span>BUSINESS LISTING</span></h2>
    <!--BREADCRUMB-->
      <ul class="breadcrumb t-breadcrumb-page">
        <li><a href="dashboard.php">Home</a></li>
        <li class="active"> Business Listing</li>
      </ul>
    <!--END BREADCRUMB-->
</div>

<div class="l-spaced">
  <h4 class="l-page-title"><span></span></h4>
    <div class="l-row mt-10 l-spaced-bottom">
        <div class="l-col-md-12 text-right">
             <div class="btn-group">
                <a href="#" data-toggle="modal" data-target="#inviteBusiiness" class="btn btn-dark"><i class="fa fa-plus"> &nbsp;</i>Invite Business</a>
            </div>
            <div class="btn-group">
               <a href="#" class="btn btn-dark"><i class=" fa fa-user"><i class=" fa fa-minus"> &nbsp;</i></i> Suspend a Business</a> &nbsp;
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-success dropdown-toggle"><i class="fa fa-refresh"> &nbsp;</i>Refresh</button>
            </div>
            <div class="btn-group">
                 <!-- <button type="button" data-toggle="modal" data-target="#smallModalExport" class="btn btn-info dropdown-toggle"><i class="fa fa-download"> &nbsp;</i> Export Users</button> -->
                 <a  data-toggle="modal" data-target="#smallModalExport" class="btn btn-info dropdown-toggle"><i class="fa fa-download"> &nbsp;</i> Export Users</a>
            </div>
        </div>
    </div>

  <!--Datatable-->
    <div id="tables" class="resp-tabs-skin-1">
        <div class="">
            <!-- Default Table-->
            <div class="l-row l-spaced-bottom">
                <div class="l-box">
                    <div class="l-box-body table-responsive">
                        <table id="dataTableId" cellspacing="0" width="100%" class="display">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" id="checkAll"/></th>
                                    <th class="col-md-8">Register Date & time</th>
                                    <th>Business Name </th>
                                    <th>Status</th>
                                    <th>Business Category</th>
                                    <th class="col-md-3" >Rating</th>
                                    <th>Review</th>
                                    <th>Phone Number</th>
                                    <th>eMail Address</th>
                                    <th>Governorates</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="checkbox"/></td>
                                    <td>21-01-2021 6:40:11 AM</td>
                                    <td><a href="business-details-page.php">Food Market</a><!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>Active</td>
                                    <td>Restaurants</td>
                                    <td ><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star"></span></td>
                                    <td>100</td>
                                    <td>+965 87542625</td>
                                    <td>info@foodmarket.com</td>
                                    <td>C.G.Road</td>
                                </tr>
                                 <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>25-01-2021 6:40:11 AM</td>
                                    <td><a href="business-details-page.php">Shayam Radh </a><!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>Active</td>
                                    <td>Restaurants</td>
                                    <td><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span></td>
                                    <td>25</td>
                                    <td>+965 69252625</td>
                                    <td>info@shyamradh.com</td>
                                    <td>C.T.M</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>22-12-2020 6:40:11 AM</td>
                                    <td><a href="business-details-page.php">Ithaa
                                    </a> <!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>Active</td>
                                    <td>Restaurants</td>
                                    <td><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></td>
                                    <td>200</td>
                                    <td>+965 67345225</td>
                                    <td>support@ithaa.com</td>
                                    <td>Maldives</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>22-12-2020 6:40:11 AM</td>
                                    <td><a href="business-details-page.php">Ithaa
                                    </a> <!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>Active</td>
                                    <td>Restaurants</td>
                                    <td><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></td>
                                    <td>200</td>
                                    <td>+965 67345225</td>
                                    <td>support@ithaa.com</td>
                                    <td>Maldives</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>22-12-2020 6:40:11 AM</td>
                                    <td><a href="business-details-page.php">Ithaa
                                    </a> <!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>Active</td>
                                    <td>Restaurants</td>
                                    <td><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></td>
                                    <td>200</td>
                                    <td>+965 67345225</td>
                                    <td>support@ithaa.com</td>
                                    <td>Maldives</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>22-12-2020 6:40:11 AM</td>
                                    <td><a href="business-details-page.php">Ithaa
                                    </a> <!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>Active</td>
                                    <td>Restaurants</td>
                                    <td><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></td>
                                    <td>200</td>
                                    <td>+965 67345225</td>
                                    <td>support@ithaa.com</td>
                                    <td>Maldives</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>22-12-2020 6:40:11 AM</td>
                                    <td><a href="business-details-page.php">Ithaa
                                    </a> <!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>Active</td>
                                    <td>Restaurants</td>
                                    <td><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></td>
                                    <td>200</td>
                                    <td>+965 67345225</td>
                                    <td>support@ithaa.com</td>
                                    <td>Maldives</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>22-12-2020 6:40:11 AM</td>
                                    <td><a href="business-details-page.php">Ithaa
                                    </a> <!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>Active</td>
                                    <td>Restaurants</td>
                                    <td><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></td>
                                    <td>200</td>
                                    <td>+965 67345225</td>
                                    <td>support@ithaa.com</td>
                                    <td>Maldives</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>22-12-2020 6:40:11 AM</td>
                                    <td><a href="business-details-page.php">Ithaa
                                    </a> <!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>Active</td>
                                    <td>Restaurants</td>
                                    <td><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></td>
                                    <td>200</td>
                                    <td>+965 67345225</td>
                                    <td>support@ithaa.com</td>
                                    <td>Maldives</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>22-12-2020 6:40:11 AM</td>
                                    <td><a href="business-details-page.php">Ithaa
                                    </a> <!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>Active</td>
                                    <td>Restaurants</td>
                                    <td><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></td>
                                    <td>200</td>
                                    <td>+965 67345225</td>
                                    <td>support@ithaa.com</td>
                                    <td>Maldives</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>22-12-2020 6:40:11 AM</td>
                                    <td><a href="business-details-page.php">Ithaa
                                    </a> <!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>Active</td>
                                    <td>Restaurants</td>
                                    <td><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></td>
                                    <td>200</td>
                                    <td>+965 67345225</td>
                                    <td>support@ithaa.com</td>
                                    <td>Maldives</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>22-12-2020 6:40:11 AM</td>
                                    <td><a href="business-details-page.php">Ithaa
                                    </a> <!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>Active</td>
                                    <td>Restaurants</td>
                                    <td><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></td>
                                    <td>200</td>
                                    <td>+965 67345225</td>
                                    <td>support@ithaa.com</td>
                                    <td>Maldives</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>22-12-2020 6:40:11 AM</td>
                                    <td><a href="business-details-page.php">Ithaa
                                    </a> <!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>Active</td>
                                    <td>Restaurants</td>
                                    <td><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></td>
                                    <td>200</td>
                                    <td>+965 67345225</td>
                                    <td>support@ithaa.com</td>
                                    <td>Maldives</td>
                                </tr>

                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Datatable-->
</div>
<!--Content Area-->
<div id="smallModalExport" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 id="smallModalLabel" class="modal-title">Export Business</h4>
            </div>
            <div class="modal-body">
                <h4><label>Are You Sure You Want To Export Business?</label></h4>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-danger">No</button>
                <button type="button" class="btn btn-success">Yes</button>
            </div>
        </div>
    </div>
</div>
<!-- jQuery-->
<script src="{{ asset('assets/js/basic/jquery.min.js')}}"></script>
<script src="{{ asset('assets/js/basic/jquery-migrate.min.js')}}"></script>
<script src="{{ asset('assets/js/calls/part.header.1.js')}}"></script>
<script src="{{ asset('assets/js/calls/part.sidebar.2.js')}}"></script>
<script src="{{ asset('assets/js/calls/part.theme.setting.js')}}"></script>
<script src="{{ asset('assets/js/calls/table.data.js')}}"></script>
<script src="{{ asset('assets/js/plugins/table/dataTables.autoFill.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/table/dataTables.colReorder.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/table/dataTables.colVis.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/table/dataTables.responsive.min.js')}}"></script>
<!-- jQuery-->
<!-- General-->
<script src="{{ asset('assets/js/basic/modernizr.min.js')}}"></script>
<script src="{{ asset('assets/js/basic/bootstrap.min.js')}}"></script>
<script src="{{ asset('assets/js/shared/jquery.asonWidget.js')}}"></script>
<script src="{{ asset('assets/js/plugins/plugins.js')}}"></script>
<script src="{{ asset('assets/js/general.js')}}"></script>

<script src="{{ asset('assets/js/plugins/pageprogressbar/pace.min.js')}}"></script>
<!-- Specific-->
<script src="{{ asset('assets/js/shared/classie.js')}}"></script>
<script src="{{ asset('assets/js/shared/jquery.cookie.min.js')}}"></script>
<script src="{{ asset('assets/js/shared/moment.min.js')}}"></script>
<script src="{{ asset('assets/js/shared/perfect-scrollbar.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/accordions/jquery.collapsible.min.js')}}"></script>


<script type="text/javascript">
     $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });
</script>
<!-- Modal: invite user -->
<div class="modal fade right" id="inviteBusiiness" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" class="modal fade"
    aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-lg modal-full-height modal-right modal-notify modal-info" role="document">
      <div class="modal-content">
        <!--Header-->
        <div class="modal-header">
            <button type="button" data-dismiss="modal" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 id="smallModalLabel" class="modal-title">INVITE Business</h4>
        </div>
       <!--Body-->
        <div class="modal-body">
            <div>
            <input  name="inviteuser" type="radio"  value="" >
            <label  class="form-check-label">Invite Through SMS</label>&nbsp;
            <input  name="inviteuser" type="radio"  value="">
            <label  class="form-check-label" >Invite Through Email</label>
            </div>
          <!-- Radio -->
         <br>
          <div>
             <select name="phone number">
                <option value="(+965) Kuwait">(+965) Kuwait</option>
             </select>
          </div>
          <br>
          <!-- textarea-->
          <div class="md-form">
            <textarea type="text"  class="md-textarea form-control" placeholder="Enter Number with comma separator.Please don't add a country code." row="5"></textarea>
          </div>
          <br>
           or 
           <div>
           <th><i class="fa fa-file" aria-hidden="true"></i> import via CSV.</th> &nbsp;<a>(click here to dowload format)</a>
            <br>
            <th>File Name: &nbsp;&nbsp;&nbsp;&nbsp;<a href="C:\Users\XPOSS_PC\Desktop\sample.csv\" target="_blank"><b>filename.csv</b></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-times" aria-hidden="true"></i></th>
           </div>
        </div>
        <!--Footer-->
        <div class="modal-footer justify-content-center">
        	<button type="submit" class="mb-5 btn btn-md btn-info btn-eff btn-eff-2"><span>Validate & Invite</span></button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal: inviteuser -->
  @endsection