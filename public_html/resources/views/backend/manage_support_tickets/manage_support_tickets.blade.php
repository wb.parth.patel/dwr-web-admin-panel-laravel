@extends('backends_layouts.master',['title'=>'SupportTicket Management'])
@section('content')
<!--Content Area-->
<div class="l-page-header">
	<div class="row">
		<div class="col-md-4">
			<h2 class="l-page-title"><span> SUPPORT TICKET'S </span></h2>
			<!--BREADCRUMB-->
      		<ul class="breadcrumb t-breadcrumb-page">
        		<li><a href="dashboard.php">Home</a></li>
        		<li class="active">Manage Support Ticket</li>
      		</ul>
      		<!--END BREADCRUMB-->			
		</div>
		   <div class="col-md-2 mt-10">
    			<small>TOTAL TICKET'S</small>
                <h4><b>50,000,000</b></h4>
		   </div>
		   <div class="col-md-2 mt-10">
    			<small>LOCATION UPDATE TICKET'S</small>
                <h4><b>100,000</b></h4>
		    </div>
		    <div class="col-md-2 mt-10">
    			<small>ADD NEW LOCATION TICKET'S</small>
                <h4><b>50,000</b></h4>
		    </div>
		    <div class="col-md-2 mt-10">
    			<small>OTHER SUPPORT TICKET'S</small>
                <h4><b>50,000</b></h4>
		    </div>
                <div class="col-md-2 mt-10">
                    <small>OPEN TICKET'S</small>
                    <h4><b>100,000</b></h4>
                </div>
                <div class="col-md-2 mt-10">
                    <small>INPROCESS TICKET'S</small>
                    <h4><b>50,000</b></h4>
                </div>
                <div class="col-md-2 mt-10">
                    <small>CLOSE TICKET'S</small>
                    <h4><b>50,000</b></h4>
                </div>
	</div>  
</div>
<div class="l-spaced">
	<h4 class="l-page-title"><span></span></h4>
    <div class="l-row mt-10 l-spaced-bottom">
        <div class="l-col-md-12 text-right">
            <div class="btn-group">
                <button href="#" data-toggle="modal" data-target="#supportticket" class="btn btn-dark"><i class="fa fa-plus"> &nbsp;</i> Create Ticket</button>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-success dropdown-toggle"><i class="fa fa-refresh"> &nbsp;</i>Refresh</button>
            </div>
            <div class="btn-group">
                <button type="button" data-toggle="modal" data-target="#smallModalExport" class="btn btn-info dropdown-toggle"><i class="fa fa-download"> &nbsp;</i> Export User </button>
            </div>
        </div>
    </div>
    <!--Datatable-->
	<div id="tables" class="resp-tabs-skin-1">
	    <div class="">
	        <!-- Default Table-->
            <div class="l-row l-spaced-bottom">
                <div class="l-box">
                    <div class="l-box-body table-responsive">
                        <table id="dataTableId" cellspacing="0" width="100%" class="display">
                            <thead>
                                <tr>
                                    <th>Date and Time</th>
							        <th>Support Ticket Title</th>
							        <th>Status</th>
							        <th>Support Ticket Type</th>
							        <th>Assign To</th>
							        <th>Due Days</th>
							        <th>Created By</th>
							        <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>21-01-2021 08:49:51 AM</td>
							        <td><a href="support-tickets-details.php">Business Name</a></td>
							        <td>Open</td>
							        <td>Location Update</td>
							        <td>Not Assigned</td>
							        <td><small>1 Day's Before</small></td>
							        <td>Amin</td>
							        <td>
							        	<div class="btn-group">
                                            <button type="button" data-toggle="dropdown" class="btn btn-drop btn-dark dropdown-toggle">Action<span class="caret"></span></button>
                                            <ul role="menu" class="dropdown-menu drop-dark">
                                                <li><a href="#" data-toggle="modal" data-target="#editsmallModal"> Edit</a></li>
                                                <li><a href="#" data-toggle="modal" data-target="#closesmallModal">Close</a></li>
                                                <li><a href="#" data-toggle="modal" data-target="#smallModalArchived">Archived</a></li>
                                            </ul>
                                        </div>
							        </td>
                                </tr>
                               	<tr>
                                    <td>21-01-2021 08:49:51 AM</td>
							        <td><a href="support-tickets-details.php">Business Name</a></td>
							        <td>Open</td>
							        <td>Location Update</td>
							        <td>Not Assigned</td>
							        <td><small>1 Day's Before</small></td>
							        <td>Amin</td>
							        <td>
							        	<div class="btn-group">
                                            <button type="button" data-toggle="dropdown" class="btn btn-drop btn-dark dropdown-toggle" aria-expanded="true">Action<span class="caret"></span></button>
                                            <ul role="menu" class="dropdown-menu drop-dark">
                                                <li><a href="#" data-toggle="modal" data-target="#smallModal">Change Status</a></li>
                                                <li><a href="#" data-toggle="modal" data-target="#editsmallModal">Edit</a></li>
                                                <li><a href="#" data-toggle="modal" data-target="#smallModalDelete">Delete</a></li>
                                            </ul>
                                        </div>
							        </td>
                                </tr>
                                <tr>
                                    <td>21-01-2021 08:49:51 AM</td>
							        <td><a href="support-tickets-details.php">Business Name</a></td>
							        <td>Open</td>
							        <td>Location Update</td>
							        <td>Not Assigned</td>
							        <td><small>1 Day's Before</small></td>
							        <td>Amin</td>
							        <td>
							        	<div class="btn-group">
                                            <button type="button" data-toggle="dropdown" class="btn btn-drop btn-dark dropdown-toggle">Action<span class="caret"></span></button>
                                            <ul role="menu" class="dropdown-menu drop-dark">
                                                <li><a href="#" data-toggle="modal" data-target="#smallModal"> Edit </a></li>
                                                <li><a href="#" data-toggle="modal" data-target="#editsmallModal">Close</a></li>
                                                <li><a href="#" data-toggle="modal" data-target="#smallModalDelete">Archived</a></li>
                                            </ul>
                                        </div>
							        </td>
                                </tr>
                                <tr>
                                    <td>21-01-2021 08:49:51 AM</td>
							        <td><a href="support-tickets-details.php">Business Name</a></td>
							        <td>Open</td>
							        <td>Add New Location</td>
							        <td>Not Assigned</td>
							        <td><small>1 Day's Before</small></td>
							        <td>Amin</td>
							        <td>
							        	<div class="btn-group">
                                            <button type="button" data-toggle="dropdown" class="btn btn-drop btn-dark dropdown-toggle">Action<span class="caret"></span></button>
                                            <ul role="menu" class="dropdown-menu drop-dark">
                                                <li><a href="#" data-toggle="modal" data-target="#smallModal"> Edit </a></li>
                                                <li><a href="#" data-toggle="modal" data-target="#editsmallModal">Close</a></li>
                                                <li><a href="#" data-toggle="modal" data-target="#smallModalDelete">Archived</a></li>
                                            </ul>
                                        </div>
							        </td>
                                </tr>
                                <tr>
                                    <td>21-01-2021 08:49:51 AM</td>
							        <td><a href="support-tickets-details.php">Business Name</a></td>
							        <td>InProcess</td>
							        <td>Add New Location</td>
							        <td>Jack James</td>
							        <td><small>1 Day's Before</small></td>
							        <td>Asimah</td>
							        <td>
							        	<div class="btn-group">
                                            <button type="button" data-toggle="dropdown" class="btn btn-drop btn-dark dropdown-toggle">Action<span class="caret"></span></button>
                                            <ul role="menu" class="dropdown-menu drop-dark">
                                                <li><a href="#" data-toggle="modal" data-target="#"> Edit </a></li>
                                                <li><a href="#" data-toggle="modal" data-target="#">Close</a></li>
                                                <li><a href="#" data-toggle="modal" data-target="#">Archived</a></li>
                                            </ul>
                                        </div>
							        </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
	    </div>
	</div>
	<!--Datatable-->
</div>
	<!-- Modal: Create Support Ticket -->
  <div class="modal fade right" id="supportticket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" class="modal fade"
    aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-lg modal-full-height modal-right modal-notify modal-info" role="document">
      <div class="modal-content">
        <!--Header-->
        <div class="modal-header">
            <button type="button" data-dismiss="modal" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 id="smallModalLabel" class="modal-title">CREATE SUPPORT TICKET </h4>
        </div>
        <!--Body-->
        <div class="modal-body">
        	<form>
        		<div class="form-group">
        			<input type="text" class="form-control" id="create-offers" placeholder="Support Title">
        		</div>
        		<div class="form-group">
            		<input type="text" class="form-control" id="create-offers" placeholder="Select Support Ticket Type">
        		</div>
        		<div class="form-group">
            		<textarea type="text"  class="form-control" id="create-offers" row="10" placeholder="Details">
            		</textarea>
        		</div>
            	<div class="form-group text-center">
        			<button type="submit" class="mb-5 btn btn-md btn-info btn-eff btn-eff-2"><span>Create Ticket</span></button>
        		</div>
        	</form>
      </div>
    </div>
  </div><!-- END Modal: Create Support Ticket -->
  
<!--Content Area-->
@endsection
<!-- jQuery-->
<script src="{{ asset('assets/js/basic/jquery.min.js')}}"></script>
<script src="{{ asset('assets/js/basic/jquery-migrate.min.js')}}"></script>
<script src="{{ asset('assets/js/calls/part.header.1.js')}}"></script>
<script src="{{ asset('assets/js/calls/part.sidebar.2.js')}}"></script>
<script src="{{ asset('assets/js/calls/part.theme.setting.js')}}"></script>
<script src="{{ asset('assets/js/calls/table.data.js')}}"></script>
<script src="{{ asset('assets/js/plugins/table/dataTables.autoFill.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/table/dataTables.colReorder.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/table/dataTables.colVis.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/table/dataTables.responsive.min.js')}}"></script>