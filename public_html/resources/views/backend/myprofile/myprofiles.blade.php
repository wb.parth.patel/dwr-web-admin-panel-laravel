@extends('backends_layouts.master',['title'=>'Profile Management'])
@section('content')

<div class="l-page-header">
    <h2 class="l-page-title"><span>Profile</span> Setting</h2>
    <!--BREADCRUMB-->
    <ul class="breadcrumb t-breadcrumb-page">
        <li><a href="dashboard.php">Home</a></li>
        <li class="active">Profile</li>
    </ul>
</div>
<div class="l-spaced">
    <div class="profile-content">
        <!-- Profile Tabs-->
        <div id="profile" class="profile-tabs">
            <div class="resp-tabs-container">
                <!--Profile-->
                <div class="profile-details-cont profile-details-cont resp-tab-content resp-tab-content-active">
                    <div class="profile-details">
                        <form role="form" class="form-horizontal">
                            <div class="l-row">
                                <div class="l-col-md-6">
                                    <h4 class="sep-bottom">General</h4>
                                    <div class="form-group">
                                        <label for="firstname" class="col-sm-3 control-label">Name</label>
                                        <div class="col-sm-9">
                                            <input id="firstname" type="text" placeholder="Full Name" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-sm-3 control-label">Email</label>
                                        <div class="col-sm-9">
                                            <input id="email" type="email" placeholder="Email" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone" class="col-sm-3 control-label">Phone</label>
                                        <div class="col-sm-9">
                                            <input id="phone" type="text" placeholder="Phone" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="l-col-md-6">
                                    <h4 class="sep-bottom">Profile Image</h4>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <div data-provides="fileinput" class="fileinput fileinput-new">
                                                <div style="width: 120px; height: 120px;" class="fileinput-new thumbnail">
                                                    <img src="{{ asset('assets/img/profile/profile.jpg')}}">
                                                </div>
                                                <div style="max-width: 120px; max-height: 120px;" class="fileinput-preview fileinput-exists thumbnail"></div>
                                                <div>
                                                    <span class="btn btn-dark btn-file"><span class="fileinput-new">Upload image</span><span class="fileinput-exists">Change</span>
                                                        <input type="file" name="..."></span><a href="#" data-dismiss="fileinput" class="btn btn-default fileinput-exists">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="l-row">
                                <h4 class="sep-bottom">Change Password</h4>
                                <div class="l-col-md-6">
                                    <div class="form-group">
                                        <label for="password" class="col-sm-3 control-label">Current Password</label>
                                        <div class="col-sm-9">
                                            <input id="password" type="password" placeholder="Current Password" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="col-sm-3 control-label">New Password</label>
                                        <div class="col-sm-9">
                                            <input id="password" type="password" placeholder="New Password" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="repPassword" class="col-sm-3 control-label">Repeat password</label>
                                        <div class="col-sm-9">
                                            <input id="repPassword" type="password" placeholder="Repeat password" class="form-control">
                                        </div>
                                    </div> 
                                    
                                </div>
                            </div>
                            <hr>
                            <div class="form-group pull-right">
                                <button type="button" class="mb-5 btn btn-lg btn-info btn-eff btn-eff-2"><span>Update</span></button>
                                <a href="dashboard.php">
                                    <button type="button" class="mb-5 btn btn-lg btn-dark btn-eff btn-eff-2"><span>Cancel</span></button>
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection