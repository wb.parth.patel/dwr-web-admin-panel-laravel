@extends('backends_layouts.master',['title'=>'Dashboard Management'])
@section('content')
 <!--Main Content-->
<!-- Row 1 - Page Summary Info-->
<!-- Page Summary Widget-->
<div class="widget-page-summary">
    <div class="l-col-lg-4">
        <h2 class="page-title">Welcome to <span>DWR</span>.</h2>
        <!-- <h4 class="page-sub-title">Your
            <span id="rotating-text">Responsive, REM font based, Clean, HTML5 and CSS3 based, Compiled with Jade, SMACSS based, Modern, Bootstrap based </span> Admin Web App.</h4> --><a href="#" class="page-summary-info-switcher"><i class="fa fa-bars"></i></a>
    </div>
    <div class="l-col-lg-8 page-summary-info">
        <!-- Page Summary Settings-->
        <div class="page-summary-settings">
            <ul class="update-status-settings">
                <li><a href="#" title="Update charts" class="chart-toggle tt-bottom"><i class="fa fa-refresh"></i></a></li>
                <li>
                    <ul class="time-status-toggle">
                        <li title="Toggle unit" class="tt-bottom">
                            <div class="hide switcheryUnits"></div>
                            <input id="switcheryUnits" type="checkbox">
                        </li>
                        <li class="last-status"><a href="#" title="Use your location" class="current-weather-location tt-bottom"><i class="fa fa-compass"></i></a></li>
                    </ul>
                </li>
                <li class="last">
                    <div class="hide switcherySettings"></div>
                    <input id="switcherySettings" type="checkbox">
                </li>
            </ul>
        </div>
        <div class="l-row">
            <!-- Page Summary Charts-->
            <div class="summary-chart chart-views l-col-md-4">
                <div class="l-row">
                    <div class="l-col-xl-5 l-col-md-6 l-col-sm-5">
                        <div class="chart-info"><a href="#" title="Update total users" class="update-chart-views tt-top"><i class="fa fa-users"></i></a><span>2,453</span>
                            <p>Total Users</p>
                        </div>
                    </div>
                    <div class="l-col-xl-7 l-col-md-6 l-col-sm-7">
                        <div class="hide rickshaw-views"></div>
                        <div id="rickshawViews"></div>
                    </div>
                </div>
            </div>
            <div class="summary-chart chart-followers l-col-md-4">
                <div class="l-row">
                    <div class="l-col-sm-5">
                        <div class="chart-info"><a href="#" title="Update Hospitals" class="update-chart-followers tt-top"><i class="fa fa-hospital-o"></i></a><span>473</span>
                            <p>Total Business</p>
                        </div>
                    </div>
                    <div class="l-col-sm-7">
                        <div class="hide rickshaw-followers"></div>
                        <div id="rickshawFollowers"></div>
                    </div>
                </div>
            </div>
            <div class="summary-chart chart-comments l-col-md-4">
                <div class="l-row">
                    <div class="l-col-sm-5">
                        <div class="chart-info"><a href="#" title="Update Clinics" class="update-chart-comments tt-top"><i class="fa fa-list"></i></a><span>695</span>
                            <p>Total Report As Spam</p>
                        </div>
                    </div>
                    <div class="l-col-sm-7">
                        <div class="hide rickshaw-comments"></div>
                        <div id="rickshawComments"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="l-row">
            <!-- Page Summary Clock-->
            <div class="summary-time-status clock-wrapper l-col-md-8 l-col-sm-6">
                <div id="clock"></div>
            </div>
            <!-- Page Summary Weather-->
            <div class="summary-time-status weather-wrapper l-col-md-4 l-col-sm-6">
                <div id="weather">
                    <div class="l-span-sm-6 l-span-xs-12">
                        <div class="weather-location">Al Ahmadi</div>
                        <div class="weather-description">Kuwait</div>
                    </div>
                    <div class="l-span-sm-3 l-span-xs-3">
                        <div class="weather-icon"><i class="ac ac-0"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Row 2 - Tabs Statistic-->
<div class="l-spaced-vertical group">
    <!-- Widget Tabs 2-->
    <div class="widget-tabs-2">
        <div class="hide tab-chart-track-color"></div>
        <ul>
            <li class="l-span-lg-3 l-span-sm-6">
                <div class="l-span-xs-7">
                    <div class="tab-2-title">Website Users</div>
                    <div class="tab-2-stat">1532<span class="text-danger"></span></div>
                    <div class="tab-2-info">Total</div>
                </div>
                <div class="l-span-xs-5 tab-chart-wrapper">
                    <div title="Update" class="tabChartUpdate2 tab-chart-update tt-top"><i class="fa fa-refresh"></i></div><span data-percent="32" class="tabChart2 tab-chart"><span class="percent"></span><i class="tab-chart-2-color fa fa-line-chart"></i></span>
                </div>
            </li>
            <li class="l-span-lg-3 l-span-sm-6">
                <div class="l-span-xs-7">
                    <div class="tab-2-title">iOs App Users</div>
                    <div class="tab-2-stat">682</div>
                    <div class="tab-2-info">Total</div>
                </div>
                <div class="l-span-xs-5 tab-chart-wrapper">
                    <div title="Update" class="tabChartUpdate3 tab-chart-update tt-top"><i class="fa fa-refresh"></i></div><span data-percent="54" class="tabChart3 tab-chart"><span class="percent"></span><i class="tab-chart-3-color fa fa-apple"></i></span>
                </div>
            </li>
            <li class="l-span-lg-3 l-span-sm-6">
                <div class="l-span-xs-7">
                    <div class="tab-2-title">Android App Users</div>
                    <div class="tab-2-stat">24,567<span class="text-success"></span></div>
                    <div class="tab-2-info">Total</div>
                </div>
                <div class="l-span-xs-5 tab-chart-wrapper">
                    <div title="Update" class="tabChartUpdate4 tab-chart-update tt-top"><i class="fa fa-refresh"></i></div><span data-percent="45" class="tabChart4 tab-chart"><span class="percent"></span><i class="tab-chart-4-color fa fa-android"></i></span>
                </div>
            </li>
            <li class="l-span-lg-3 l-span-sm-6">
                <div class="l-span-xs-7">
                    <div class="tab-2-title">Total App Users</div>
                    <div class="tab-2-stat">745<span class="text-success"></span></div>
                    <div class="tab-2-info">Total</div>
                </div>
                <div class="l-span-xs-5 tab-chart-wrapper">
                    <div title="Update" class="tabChartUpdate1 tab-chart-update tt-top"><i class="fa fa-refresh"></i></div><span data-percent="75" class="tabChart1 tab-chart"><span class="percent"></span><i class="tab-chart-1-color fa fa-stethoscope"></i></span>
                </div>
            </li>
        </ul>
        <ul>
            <li class="l-span-lg-3 l-span-sm-6">
                <div class="l-span-xs-7">
                    <div class="tab-2-title">Register App Users</div>
                    <div class="tab-2-stat">745<span class="text-success"></span></div>
                    <div class="tab-2-info">Total</div>
                </div>
                <div class="l-span-xs-5 tab-chart-wrapper">
                    <div title="Update" class="tabChartUpdate1 tab-chart-update tt-top"><i class="fa fa-refresh"></i></div><span data-percent="75" class="tabChart1 tab-chart"><span class="percent"></span><i class="tab-chart-1-color fa fa-stethoscope"></i></span>
                </div>
            </li>
            <li class="l-span-lg-3 l-span-sm-6">
                <div class="l-span-xs-7">
                    <div class="tab-2-title">Unregister App Users</div>
                    <div class="tab-2-stat">1532<span class="text-danger"></span></div>
                    <div class="tab-2-info">Total</div>
                </div>
                <div class="l-span-xs-5 tab-chart-wrapper">
                    <div title="Update" class="tabChartUpdate2 tab-chart-update tt-top"><i class="fa fa-refresh"></i></div><span data-percent="32" class="tabChart2 tab-chart"><span class="percent"></span><i class="tab-chart-2-color fa fa-line-chart"></i></span>
                </div>
            </li>
            <li class="l-span-lg-3 l-span-sm-6">
                <div class="l-span-xs-7">
                    <div class="tab-2-title">Total Business</div>
                    <div class="tab-2-stat">682</div>
                    <div class="tab-2-info">Total</div>
                </div>
                <div class="l-span-xs-5 tab-chart-wrapper">
                    <div title="Update" class="tabChartUpdate3 tab-chart-update tt-top"><i class="fa fa-refresh"></i></div><span data-percent="54" class="tabChart3 tab-chart"><span class="percent"></span><i class="tab-chart-3-color fa fa-apple"></i></span>
                </div>
            </li>
            <li class="l-span-lg-3 l-span-sm-6">
                <div class="l-span-xs-7">
                    <div class="tab-2-title">Approved Business</div>
                    <div class="tab-2-stat">24,567<span class="text-success"></span></div>
                    <div class="tab-2-info">Total</div>
                </div>
                <div class="l-span-xs-5 tab-chart-wrapper">
                    <div title="Update" class="tabChartUpdate4 tab-chart-update tt-top"><i class="fa fa-refresh"></i></div><span data-percent="45" class="tabChart4 tab-chart"><span class="percent"></span><i class="tab-chart-4-color fa fa-android"></i></span>
                </div>
            </li>
        </ul>
        <ul>
            <li class="l-span-lg-3 l-span-sm-6">
                <div class="l-span-xs-7">
                    <div class="tab-2-title">Business Request</div>
                    <div class="tab-2-stat">745<span class="text-success"></span></div>
                    <div class="tab-2-info">Total</div>
                </div>
                <div class="l-span-xs-5 tab-chart-wrapper">
                    <div title="Update" class="tabChartUpdate1 tab-chart-update tt-top"><i class="fa fa-refresh"></i></div><span data-percent="75" class="tabChart1 tab-chart"><span class="percent"></span><i class="tab-chart-1-color fa fa-stethoscope"></i></span>
                </div>
            </li>
            <li class="l-span-lg-3 l-span-sm-6">
                <div class="l-span-xs-7">
                    <div class="tab-2-title">Block Business</div>
                    <div class="tab-2-stat">1532<span class="text-danger"></span></div>
                    <div class="tab-2-info">Total</div>
                </div>
                <div class="l-span-xs-5 tab-chart-wrapper">
                    <div title="Update" class="tabChartUpdate2 tab-chart-update tt-top"><i class="fa fa-refresh"></i></div><span data-percent="32" class="tabChart2 tab-chart"><span class="percent"></span><i class="tab-chart-2-color fa fa-line-chart"></i></span>
                </div>
            </li>
            <li class="l-span-lg-3 l-span-sm-6">
                <div class="l-span-xs-7">
                    <div class="tab-2-title">Update Location</div>
                    <div class="tab-2-stat">682</div>
                    <div class="tab-2-info">Total</div>
                </div>
                <div class="l-span-xs-5 tab-chart-wrapper">
                    <div title="Update" class="tabChartUpdate3 tab-chart-update tt-top"><i class="fa fa-refresh"></i></div><span data-percent="54" class="tabChart3 tab-chart"><span class="percent"></span><i class="tab-chart-3-color fa fa-apple"></i></span>
                </div>
            </li>
            <li class="l-span-lg-3 l-span-sm-6">
                <div class="l-span-xs-7">
                    <div class="tab-2-title">Report As Spam</div>
                    <div class="tab-2-stat">24,567<span class="text-success"></span></div>
                    <div class="tab-2-info">Total</div>
                </div>
                <div class="l-span-xs-5 tab-chart-wrapper">
                    <div title="Update" class="tabChartUpdate4 tab-chart-update tt-top"><i class="fa fa-refresh"></i></div><span data-percent="45" class="tabChart4 tab-chart"><span class="percent"></span><i class="tab-chart-4-color fa fa-android"></i></span>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- <div class="l-col-lg-12">
    <div class="l-col-lg-6">
                <figure class="highcharts-figure col-lg-6">
                <div id="container"></div>
                <p class="highcharts-description">
                    A basic column chart compares rainfall values between four cities.
                    Tokyo has the overall highest amount of rainfall, followed by New York.
                    The chart is making use of the axis crosshair feature, to highlight
                    months as they are hovered over.
                </p>
            </figure>
    </div>

    <div class="l-col-lg-6">
                <figure class="highcharts-figure col-lg-6">
                <div id="container"></div>
                <p class="highcharts-description">
                    A basic column chart compares rainfall values between four cities.
                    Tokyo has the overall highest amount of rainfall, followed by New York.
                    The chart is making use of the axis crosshair feature, to highlight
                    months as they are hovered over.
                </p>
            </figure>
    </div>
</div> -->
<figure class="highcharts-figure col-lg-6">
    <div id="container"></div>
    <!-- <p class="highcharts-description">
        A basic column chart compares rainfall values between four cities.
        Tokyo has the overall highest amount of rainfall, followed by New York.
        The chart is making use of the axis crosshair feature, to highlight
        months as they are hovered over.
    </p> -->
</figure>
<figure class="highcharts-figure col-lg-6">
    <div id="container2"></div>
    <!-- <p class="highcharts-description">
        A basic column chart compares rainfall values between four cities.
        Tokyo has the overall highest amount of rainfall, followed by New York.
        The chart is making use of the axis crosshair feature, to highlight
        months as they are hovered over.
    </p> -->
</figure>
<!-- Row 3 - Tabs Statistic-->
<div class="l-spaced-vertical">
    <!-- Widget Statistic-->
    <!-- <div class="widget-statistic">
        <div class="statistic-header">
            <div class="l-span-sm-4">
                <div id="statisticTitle" class="statistic-title">Statistics <span class="statistic-title-1">+ 2,453</span></div>
            </div>
            <div class="l-span-sm-8">
                <ul class="statistic-options">
                    <li><a id="statisticFullScreen" href="#" title="Fullscreen" data-ason-type="fullscreen" data-ason-target=".widget-statistic" data-ason-content="true" class="ason-widget tt-top"></a></li>
                    <li><a href="#" title="Refresh" data-ason-type="refresh" data-ason-target=".widget-statistic" data-ason-duration="1000" class="ason-widget tt-top"><i class="fa fa-rotate-right"></i></a></li>
                    <li><a href="#" title="Toggle" data-ason-type="toggle" data-ason-find=".widget-statistic" data-ason-target=".statistic-body" data-ason-content="true" data-ason-duration="200" class="ason-widget tt-top"></a></li>
                    <li class="last"><a href="#" title="Delete" data-ason-type="delete" data-ason-target=".widget-statistic" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget tt-top"></a></li>
                </ul>
                <ul id="statisticChartType" class="statistic-options">
                    <li><a href="#" title="Area Chart" id="statisticAreaBtn" class="tt-top active"><i class="fa fa-area-chart"></i></a></li>
                    <li><a href="#" title="Bar Chart" id="statisticBarBtn" class="tt-top"><i class="fa fa-bar-chart"></i></a></li>
                    <li><a href="#" title="Line Chart" id="statisticLineBtn" class="tt-top"><i class="fa fa-line-chart"></i></a></li>
                </ul>
                <ul id="statisticDataType" class="statistic-options">
                    <li><a href="#" title="Visitors" id="statisticVisitorsBtn" class="tt-top active"><i class="fa fa-users"></i></a></li>
                    <li><a href="#" title="Pageviews" id="statisticPageviewsBtn" class="tt-top"><i class="fa fa-file-text-o"></i></a></li>
                    <li><a href="#" title="Average Visit Time" id="statisticTimeBtn" class="tt-top"><i class="fa fa-clock-o"></i></a></li>
                    <li><a href="#" title="Bounce Rate" id="statisticBounceBtn" class="tt-top"><i class="fa fa-exchange"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="statistic-body">
            <div class="hide rickshaw-statistic-c-1-1"></div>
            <div class="hide rickshaw-statistic-c-1-2"></div>
            <div class="hide rickshaw-statistic-c-2-1"></div>
            <div class="hide rickshaw-statistic-c-3-1"></div>
            <div class="hide rickshaw-statistic-c-4-1"></div>
            <div id="statisticChart"></div>
            <div id="statisticChartLegend" class="statistic_rickshaw_legend"></div>
        </div>
    </div> -->
</div>
<div class="l-col-lg-12">
    <div class="l-spaced">
        <div class="l-col-lg-6 l-col-md-8 l-clear-md l-spaced-bottom">
            <div class="l-box-header">
                <h2 class="l-box-title"><span>Latest User Register</span></h2>
            </div>
            <div class="l-box">
                <div class="l-box-body l-row">
                    <!-- <div class="l-spaced">
                        <div class="l-row mt-10">
                            <div class="l-col-md-10">
                            </div>
                            <div class="l-col-md-2 text-right">
                                <select class="l-spaced-bottom form-control">
                                    <option value="">15</option>
                                    <option value="">25</option>
                                    <option value="">35</option>
                                </select>
                            </div>
                        </div>
                    </div> -->
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Date and Time</th>
                                <th>Full Name</th>
                                <th>Active Device</th>
                                <th>Search Volume</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>11-12-2021 08:12:20 AM</td>
                                <td>Aadidah Aadab</td>
                                <td>iPhone 12</td>
                                <td>10</td>
                                <td><i class="fa fa-search"></i>&nbsp;&nbsp; <i class="fa fa-ban"></i></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>11-12-2021 08:12:20 AM</td>
                                <td>Aadidah Aadab</td>
                                <td>iPhone 12</td>
                                <td>10</td>
                                <td><i class="fa fa-search"></i>&nbsp;&nbsp; <i class="fa fa-ban"></i></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>11-12-2021 08:12:20 AM</td>
                                <td>Aadidah Aadab</td>
                                <td>iPhone 12</td>
                                <td>10</td>
                                <td><i class="fa fa-search"></i>&nbsp;&nbsp; <i class="fa fa-ban"></i></td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>11-12-2021 08:12:20 AM</td>
                                <td>Aadidah Aadab</td>
                                <td>iPhone 12</td>
                                <td>10</td>
                                <td><i class="fa fa-search"></i>&nbsp;&nbsp; <i class="fa fa-ban"></i></td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>11-12-2021 08:12:20 AM</td>
                                <td>Aadidah Aadab</td>
                                <td>iPhone 12</td>
                                <td>10</td>
                                <td><i class="fa fa-search"></i>&nbsp;&nbsp; <i class="fa fa-ban"></i></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="l-spaced">
        <div class="l-col-lg-6 l-col-md-8 l-clear-md l-spaced-bottom" style="margin-left:10px;">
            <div class="l-box-header">
                <h2 class="l-box-title"><span>Top 20 Business</span></h2>
            </div>
            <div class="l-box">
                <div class="l-box-body l-row">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Business Name</th>
                                <th>Business Category</th>
                                <th>Profile Views</th>
                                <th>Search Volume</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Business Name</td>
                                <td>Category 1</td>    
                                <td>1200</td>
                                <td>20000</td>
                            </tr>
                            <tr>
                                <td>Business Name</td>
                                <td>Category 1</td>    
                                <td>1200</td>
                                <td>20000</td>
                            </tr>
                            <tr>
                                <td>Business Name</td>
                                <td>Category 1</td>    
                                <td>1200</td>
                                <td>20000</td>
                            </tr>
                            <tr>
                                <td>Business Name</td>
                                <td>Category 1</td>    
                                <td>1200</td>
                                <td>20000</td>
                            </tr>
                            <tr>
                                <td>Business Name</td>
                                <td>Category 1</td>    
                                <td>1200</td>
                                <td>20000</td>
                            </tr>
                            <tr>
                                <td>Business Name</td>
                                <td>Category 1</td>    
                                <td>1200</td>
                                <td>20000</td>
                            </tr>
                            <tr>
                                <td>Business Name</td>
                                <td>Category 1</td>    
                                <td>1200</td>
                                <td>20000</td>
                            </tr>
                            <tr>
                                <td>Business Name</td>
                                <td>Category 1</td>    
                                <td>1200</td>
                                <td>20000</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Row 6 - Hospital List -->
<!-- Row 5 - Appoinment Data Table ,Hospital Bar Chart -->
<div class="col-lg-6">

    <figure class="highcharts-figure">
    <div class="l-box widget">
        <div class="l-box-header" >
            <h2 class="l-box-title"><span>Support Ticket Statistic</span></h2>
        </div>
        <div class="l-box-body l-spaced">
         <div class="l-box l-row">
            <div class="l-col-lg-4 l-col-md-6 l-box-body l-row" id="TotalCampaign">
                <h4>Total Campaign</h4>
                <div class="l-row">
                
                <div class="l-col-lg-4">
                    <i class="icon-font-sm icon fa fa-arrow-up"></i><br>
                    <span class="per-text">10%</span>
                </div>
                <div class="l-col-lg-8 text-left">
                    <h1 class="mt-0">5000</h1>
                </div>
                </div>
                

            </div>
            <div class="l-col-lg-4 l-col-md-6 l-box-body l-row" id="RunningCampaign">
             <h4>Running Campaign</h4>
                <div class="l-row">
                
                <div class="l-col-lg-4">
                    <i class="icon-font-sm icon fa fa-arrow-up"></i><br>
                    <span class="per-text">10%</span>
                </div>
                <div class="l-col-lg-8 text-left">
                    <h1 class="mt-0">5000</h1>
                </div>
                </div>
            </div>
            <div class="l-col-lg-4 l-col-md-6 l-box-body l-row" id="CloseCampaign">
            <h4>Close Campaign</h4>
                <div class="l-row">
                
                <div class="l-col-lg-4">
                    <i class="icon-font-sm icon fa fa-arrow-up"></i><br>
                    <span class="per-text">10%</span>
                </div>
                <div class="l-col-lg-8 text-left">
                    <h1 class="mt-0">5000</h1>
                </div>
                </div>
            </div>
        </div>
        </div>
        <div id="container12"></div>
    </figure>
</div>
<div class="l-spaced l-spaced-bottom">
    <div class="l-col-lg-6 l-col-md- l-clear-md l-spaced-bottom">
        <div class="l-box-header">
            <h2 class="l-box-title"><span>Latest Business Request</span></h2>
        </div>
        <div class="l-box">
            <div class="l-box-body">
                <!-- <table id="dataTableId" cellspacing="0" width="100%" class="table table-bordered"> -->
                <table id="" cellspacing="0" width="100%" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Date and Time</th>
                            <th>Requested By</th>
                            <th>Business Name</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>10/02/2021 08:12:20 AM</td>
                            <td>Abid Ali</td>
                            <td>Business Name</td>
                            <td>Approved</td>
                            <td>
                                <a href="#"><i class="fa fa-search"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-check"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-ban"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>10/02/2021 08:12:20 AM</td>
                            <td>Abid Ali</td>
                            <td>Business Name</td>
                            <td>Approved</td>
                            <td>
                                <a href="#"><i class="fa fa-search"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-check"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-ban"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>10/02/2021 08:12:20 AM</td>
                            <td>Abid Ali</td>
                            <td>Business Name</td>
                            <td>Approved</td>
                            <td>
                                <a href="#"><i class="fa fa-search"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-check"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-ban"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>10/02/2021 08:12:20 AM</td>
                            <td>Abid Ali</td>
                            <td>Business Name</td>
                            <td>Approved</td>
                            <td>
                                <a href="#"><i class="fa fa-search"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-check"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-ban"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>10/02/2021 08:12:20 AM</td>
                            <td>Abid Ali</td>
                            <td>Business Name</td>
                            <td>Approved</td>
                            <td>
                                <a href="#"><i class="fa fa-search"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-check"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-ban"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>10/02/2021 08:12:20 AM</td>
                            <td>Abid Ali</td>
                            <td>Business Name</td>
                            <td>Approved</td>
                            <td>
                                <a href="#"><i class="fa fa-search"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-check"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-ban"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>10/02/2021 08:12:20 AM</td>
                            <td>Abid Ali</td>
                            <td>Business Name</td>
                            <td>Approved</td>
                            <td>
                                <a href="#"><i class="fa fa-search"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-check"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-ban"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>10/02/2021 08:12:20 AM</td>
                            <td>Abid Ali</td>
                            <td>Business Name</td>
                            <td>Approved</td>
                            <td>
                                <a href="#"><i class="fa fa-search"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-check"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-ban"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>10/02/2021 08:12:20 AM</td>
                            <td>Abid Ali</td>
                            <td>Business Name</td>
                            <td>Approved</td>
                            <td>
                                <a href="#"><i class="fa fa-search"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-check"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-ban"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>10/02/2021 08:12:20 AM</td>
                            <td>Abid Ali</td>
                            <td>Business Name</td>
                            <td>Approved</td>
                            <td>
                                <a href="#"><i class="fa fa-search"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-check"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-ban"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="l-spaced l-row">
    <div class="l-col-lg-6 l-col-md-8 l-spaced-bottom">
        <div class="l-box-header">
            <h2 class="l-box-title"><span>Support Ticket</span></h2>
        </div>
        <div class="l-box">
            <div class="l-box-body l-row">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Support Ticket Title</th>
                            <th>Status</th>
                            <th>Assigned to</th>
                            <th>Date & Time</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Business Name</td>
                            <td>In Process</td>    
                            <td>Jack Thomas</td>
                            <td>11-12-2021 08:12:20 AM</td>
                            <td>
                                <a href="#"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-check"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-close"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>Business Name</td>
                            <td>In Process</td>    
                            <td>Jack Thomas</td>
                            <td>11-12-2021 08:12:20 AM</td>
                            <td>
                                <a href="#"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-check"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-close"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>Business Name</td>
                            <td>In Process</td>    
                            <td>Jack Thomas</td>
                            <td>11-12-2021 08:12:20 AM</td>
                            <td>
                                <a href="#"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-check"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-close"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>Business Name</td>
                            <td>In Process</td>    
                            <td>Jack Thomas</td>
                            <td>11-12-2021 08:12:20 AM</td>
                            <td>
                                <a href="#"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-check"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-close"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>Business Name</td>
                            <td>In Process</td>    
                            <td>Jack Thomas</td>
                            <td>11-12-2021 08:12:20 AM</td>
                            <td>
                                <a href="#"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-check"></i></a>&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-close"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="l-col-lg-6 l-col-md-8 l-spaced-bottom">
     <div class="l-box widget">
        <div class="l-box-header" >
            <h2 class="l-box-title"><span>Marketing Campaign Information</span></h2>
        </div>
        <div class="l-box-body l-spaced">
         <div class="l-row">
            <div class="l-col-lg-4 l-col-md-6 l-box-body l-row" id="TotalCampaign">
                <h4>Total Campaign</h4>
                <div class="l-row">
                    <div class="l-col-lg-4">
                        <i class="icon-font-sm icon fa fa-arrow-up"></i><br>
                        <span class="per-text">10%</span>
                    </div>
                    <div class="l-col-lg-8 text-left">
                        <h1 class="mt-0">5000</h1>
                    </div>
                </div>
            </div>
            <div class="l-col-lg-4 l-col-md-6 l-box-body l-row" id="RunningCampaign">
             <h4>Running Campaign</h4>
                <div class="l-row">
                    <div class="l-col-lg-4">
                        <i class="icon-font-sm icon fa fa-arrow-up"></i><br>
                        <span class="per-text">10%</span>
                    </div>
                    <div class="l-col-lg-8 text-left">
                        <h1 class="mt-0">5000</h1>
                    </div>
                </div>
            </div>
            <div class="l-col-lg-4 l-col-md-6 l-box-body l-row" id="CloseCampaign">
            <h4>Close Campaign</h4>
                <div class="l-row">
                    <div class="l-col-lg-4">
                        <i class="icon-font-sm icon fa fa-arrow-up"></i><br>
                        <span class="per-text">10%</span>
                    </div>
                    <div class="l-col-lg-8 text-left">
                        <h1 class="mt-0">5000</h1>
                    </div>
                </div>
            </div>
        </div>
        </div>

        
    </div>
    <div class="l-box widget mt-10">
        <div class="l-box-header" >
            <h2 class="l-box-title"><span>Event Summary</span></h2>
        </div>
        <div class="l-box-body l-spaced">
            <div class="l-row">
                    <div class="l-col-lg-4 l-col-md-6 l-box-body l-row" id="TotalCampaign">
                        <h4>Total Event</h4>
                        <div class="l-row">
                        
                            <div class="l-col-lg-4">
                                <i class="icon-font-sm icon fa fa-arrow-up"></i><br>
                                <span class="per-text">10%</span>
                            </div>
                            <div class="l-col-lg-8 text-left">
                                <h1 class="mt-0">5000</h1>
                            </div>
                        </div>  
                    </div>
                <div class="l-col-lg-4 l-col-md-6 l-box-body l-row" id="RunningCampaign">
                        <h4>Running Event</h4>
                        <div class="l-row">
                            <div class="l-col-lg-4">
                                <i class="icon-font-sm icon fa fa-arrow-up"></i><br>
                                <span class="per-text">10%</span>
                            </div>
                            <div class="l-col-lg-8 text-left">
                                <h1 class="mt-0">5000</h1>
                            </div>
                        </div>
                </div>
                <div class="l-col-lg-4 l-col-md-6 l-box-body l-row" id="CloseCampaign">
                    <h4>Close Event</h4>
                    <div class="l-row">
                        <div class="l-col-lg-4">
                            <i class="icon-font-sm icon fa fa-arrow-up"></i><br>
                            <span class="per-text">10%</span>
                        </div>
                        <div class="l-col-lg-8 text-left">
                            <h1 class="mt-0">5000</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection
