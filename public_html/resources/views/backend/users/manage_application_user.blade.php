@extends('backends_layouts.master',['title'=>'Application User Management'])
@section('content')

<!--Content Area-->
<div class="l-page-header">
    <h2 class="l-page-title"><span> Application Users </span></h2>
    <!--BREADCRUMB-->
      <ul class="breadcrumb t-breadcrumb-page">
        <li><a href="dashboard.php">Home</a></li>
        <li class="active">Application User</li>
      </ul>
    <!--END BREADCRUMB-->
</div>

<div class="l-spaced">
  <h4 class="l-page-title"><span></span></h4>
    <div class="l-row mt-10 l-spaced-bottom">
        <div class="l-col-md-12 text-right">
            <div class="btn-group">
                <button href="#" data-toggle="modal" data-target="#inviteuser" class="btn btn-dark"><i class="fa fa-plus"> &nbsp;</i> Invite User</button>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-success dropdown-toggle"><i class="fa fa-refresh"> &nbsp;</i>Refresh</button>
            </div>
            <div class="btn-group">
                <button type="button" data-toggle="modal" data-target="#smallModalExport" class="btn btn-info dropdown-toggle"><i class="fa fa-download"> &nbsp;</i> Export User </button>
            </div>
        </div>
    </div>

  <!--Datatable-->
	<div id="tables" class="resp-tabs-skin-1">
	    <div class="">
	        <!-- Default Table-->
            <div class="l-row l-spaced-bottom">
                <div class="l-box">
                    <div class="l-box-body table-responsive">
                        <table id="dataTableId" cellspacing="0" width="100%" class="display">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Date and Time</th>
              							        <th>User Name</th>
              							        <th>User Type</th>
              							        <th>Gender</th>
              							        <th>Phone Number</th>
              							        <th>Governorates</th>
              							        <th>Application Usages</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>21-01-2021 08:49:51 AM</td>
                							      <td><a href="user-details.php">Aabidah Aadab</a></td>
              							        <td>Register Users</td>
              							        <td>Male</td>
              							        <td>+965 5468-0985</td>
              							        <td>Hawalli</td>
              							        <td>72:56:24</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>21-01-2021 08:49:51 AM</td>
              							        <td><a href="user-details.php">Aani Fatimah Khatoon</a></td>
              							        <td>Unregister Users</td>
              							        <td>Male</td>
              							        <td>+965 5468-0985</td>
              							        <td>Farwaniyah</td>
              							        <td>200:10:20</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>21-01-2021 08:49:51 AM</td>
              							        <td><a href="user-details.php">Abdul Baasit Abdul Badee</a></td>
              							        <td>Unregister Users</td>
              							        <td>Male</td>
              							        <td>+965 5468-0985</td>
              							        <td>Ahmadi</td>
              							        <td>500:10:20</td>
                                </tr>                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
	    </div>
	</div>
	<!--Datatable-->
</div>
<!--Content Area-->
<!-- jQuery-->
<script src="{{ asset('assets/js/basic/jquery.min.js')}} "></script>
<script src="{{ asset('assets/js/basic/jquery-migrate.min.js')}} "></script>
<script src="{{ asset('assets/js/calls/part.header.1.js')}} "></script>
<script src="{{ asset('assets/js/calls/part.sidebar.2.js')}} "></script>
<script src="{{ asset('assets/js/calls/part.theme.setting.js')}} "></script>
<script src="{{ asset('assets/js/calls/table.data.js')}} "></script>
<script src="{{ asset('assets/js/plugins/table/dataTables.autoFill.min.js')}} "></script>
<script src="{{ asset('assets/js/plugins/table/dataTables.colReorder.min.js')}} "></script>
<script src="{{ asset('assets/js/plugins/table/dataTables.colVis.min.js')}} "></script>
<script src="{{ asset('assets/js/plugins/table/dataTables.responsive.min.js')}} "></script>
<!-- General-->
<script src="{{ asset('assets/js/basic/modernizr.min.js')}}"></script>
<script src="{{ asset('assets/js/basic/bootstrap.min.js')}} "></script>
<script src="{{ asset('assets/js/shared/jquery.asonWidget.js')}} "></script>
<script src="{{ asset('assets/js/plugins/plugins.js')}} "></script>
<script src="{{ asset('assets/js/general.js')}} "></script>

<script src="{{ asset('assets/js/plugins/pageprogressbar/pace.min.js')}} "></script>
<!-- Specific-->
<script src="{{ asset('assets/js/shared/classie.js')}} "></script>
<script src="{{ asset('assets/js/shared/jquery.cookie.min.js')}} "></script>
<script src="{{ asset('assets/js/shared/moment.min.js')}} "></script>
<script src="{{ asset('assets/js/shared/perfect-scrollbar.min.js')}} "></script>
<script src="{{ asset('assets/js/plugins/accordions/jquery.collapsible.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/datetime/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/datetime/bootstrap-datetimepicker.min.js')}} "></script>
<script src="{{ asset('assets/js/plugins/datetime/clockface.js')}} "></script>
<script src="{{ asset('assets/js/plugins/datetime/daterangepicker.js')}} "></script>
<script src="{{ asset('assets/js/plugins/datetime/jqClock.min.js')}} "></script>
<script src="{{ asset('assets/js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js')}} "></script>
<script src="{{ asset('assets/js/plugins/forms/elements/jquery.checkBo.min.js')}} "></script>
<script src="{{ asset('assets/js/plugins/forms/elements/jquery.switchery.min.js')}} "></script>
<script src="{{ asset('assets/js/plugins/notifications/jquery.jgrowl.min.js')}} "></script>
<script src="{{ asset('assets/js/plugins/tooltip/jquery.tooltipster.min.js')}} "></script>
<script src="{{ asset('assets/js/calls/part.header.1.js')}} "></script>
<script src="{{ asset('assets/js/calls/part.sidebar.2.js')}} "></script>
<script src="{{ asset('assets/js/calls/part.theme.setting.js')}} "></script>
<script src="{{ asset('assets/js/calls/ui.datetime.js')}} "></script>

<!-- Modal: invite user -->
  <div class="modal fade right" id="inviteuser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" class="modal fade"
    aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-lg modal-full-height modal-right modal-notify modal-info" role="document">
      <div class="modal-content">
        <!--Header-->
        <div class="modal-header">
            <button type="button" data-dismiss="modal" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 id="smallModalLabel" class="modal-title">INVITE USER</h4>
        </div>
       <!--Body-->
        <div class="modal-body">
            <div>
            <input  name="inviteuser" type="radio"  value="" >
            <label  class="form-check-label">Invite Through SMS</label>&nbsp;
            <input  name="inviteuser" type="radio"  value="">
            <label  class="form-check-label" >Invite Through Email</label>
            </div>
          <!-- Radio -->
         <br>
          <div>
             <select name="phone number">
                <option value="(+965) Kuwait">(+965) Kuwait</option>
             </select>
          </div>
          <br>
          <!-- textarea-->
          <div class="md-form">
            <textarea type="text"  class="md-textarea form-control" placeholder="Enter Number with comma separator.Please don't add a country code." row="5"></textarea>
          </div>
          <br>
           or 
           <div>
           <th><i class="fa fa-file" aria-hidden="true"></i> import via CSV.</th> &nbsp;<a>(click here to dowload format)</a>
            <br>
            <th>File Name: &nbsp;&nbsp;&nbsp;&nbsp;<a href="C:\Users\XPOSS_PC\Desktop\sample.csv\" target="_blank"><b>filename.csv</b></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-times" aria-hidden="true"></i></th>
           </div>
        </div>
        <!--Footer-->
        <div class="modal-footer justify-content-center">
        	<button type="submit" class="mb-5 btn btn-md btn-info btn-eff btn-eff-2"><span>Validate & Invite</span></button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal: inviteuser -->

@endsection