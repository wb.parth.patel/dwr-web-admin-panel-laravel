@extends('backends_layouts.master',['title'=>'Dwr Internal Users Management'])
@section('content')
<!--Content Area-->
<div class="l-page-header">
    <h2 class="l-page-title"><span> DWR INTERNAL USERS </span></h2>
    <!--BREADCRUMB-->
      <ul class="breadcrumb t-breadcrumb-page">
        <li><a href="dashboard.php">Home</a></li>
        <li class="active">Dwr Internal users</li>
      </ul>
    <!--END BREADCRUMB-->
</div>

<div class="l-spaced">
  <h4 class="l-page-title"><span></span></h4>
    <div class="l-row mt-10 l-spaced-bottom">
        <div class="l-col-md-12 text-right">
            <div class="btn-group">
                <a href="#" data-toggle="modal" data-target="#" class="btn btn-dark"><i class="fa fa-plus"> &nbsp;</i>Invite Users</a>
            </div>
            <div class="btn-group">
               <a href="#" class="btn btn-dark"><i class=" fa fa-user"><i class=" fa fa-minus"> &nbsp;</i></i> Suspend a users</a> &nbsp;
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-success dropdown-toggle"><i class="fa fa-refresh"> &nbsp;</i>Refresh</button>
            </div>
            <div class="btn-group">
                 <button type="button" data-toggle="modal" data-target="#smallModalExport" class="btn btn-info dropdown-toggle"><i class="fa fa-download"> &nbsp;</i> Export users</button>
              </div>
        </div>
    </div>

  <!--Datatable-->
    <div id="tables" class="resp-tabs-skin-1">
        <div class="">
            <!-- Default Table-->
            <div class="l-row l-spaced-bottom">
                <div class="l-box">
                    <div class="l-box-body table-responsive">
                        <table id="dataTableId" cellspacing="0" width="100%" class="display">
                            <div class="text-right">
                            <button type="button" class="md-5 btn btn-lg btn-danger"><span>
                             Delete All</span></button>
                           </div> 
                            <thead>
                                <tr>
                                    <th><input type="checkbox" id="checkAll"/><!-- <input type="checkbox" name="all[]" class="select-checkall" id="checkall" value=""> --></th>
                                    <th>Date and time</th>
                                    <th>User Name </th>
                                    <th>User Role</th>
                                    <th>Phone Number</th>
                                    <th>Email Address</th>
                                    <th>IP Address</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="checkbox" /> </td>
                                    <td>21-01-2021 6:40:11 AM</td>
                                    <td><a href="#" data-toggle="modal" data-target="#profileuser">Tonight Gamer</a><!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>support user</td>
                                    <td>+965 87542625</td>
                                    <td>gamer@gmail.com</td>
                                    <td>127.0.0.1</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /> </td>
                                    <td>25-01-2021 6:40:11 AM</td>
                                    <td><a href="#" data-toggle="modal" data-target="#profileuser">Nucking Futz</a><!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>support user</td>
                                    <td>+965 87542625</td>
                                    <td>futz@gmail.com</td>
                                    <td>127.0.1.1</td>
                                </tr>
                                 <tr>
                                    <td><input type="checkbox" /> </td>
                                    <td>22-12-2020 6:40:11 AM</td>
                                    <td><a href="#" data-toggle="modal" data-target="#profileuser">Misty Moles</a><!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>internal user</td>
                                    <td>+965 67345225</td>
                                    <td>moles@gmail.com</td>
                                    <td>127.0.1.2</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /> </td>
                                    <td>15-11-2020 6:40:11 AM</td>
                                    <td><a href="#" data-toggle="modal" data-target="#profileuser">Mollen Mist</a><!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>sub admin</td>
                                    <td>+965 27343625</td>
                                    <td>mist@gmail.com</td>
                                    <td>127.0.1.3</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>18-01-2021 6:40:11 AM</td>
                                    <td><a href="#" data-toggle="modal" data-target="#profileuser">Texas Tiger</a><!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>support user</td>
                                    <td>+965 87542625</td>
                                    <td>texas@gmail.com</td>
                                    <td>127.0.0.3</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /> </td>
                                    <td>31-01-2021 6:40:11 AM</td>
                                    <td><a href="#" data-toggle="modal" data-target="#profileuser">Million Mack</a><!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>support user</td>
                                    <td>+965 78654535</td>
                                    <td>mack@gmail.com</td>
                                    <td>127.0.0.7</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /> </td>
                                    <td>08-07-2020 6:40:11 AM</td>
                                    <td><a href="#" data-toggle="modal" data-target="#profileuser">ronald Gamer</a><!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>support user</td>
                                    <td>+965 56869235</td>
                                    <td>ronald@gmail.com</td>
                                    <td>127.0.0.55</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /> </td>
                                    <td>28-08-2020 6:40:11 AM</td>
                                    <td><a href="#" data-toggle="modal" data-target="#profileuser">Mollen Mist</a><!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>support user</td>
                                    <td>+965 97253615</td>
                                    <td>mollen@gmail.com</td>
                                    <td>127.0.10.10</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /> </td>
                                    <td>14-02-2021 6:40:11 AM</td>
                                    <td><a href="#" data-toggle="modal" data-target="#profileuser">Global Tummy</a><!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>support user</td>
                                    <td>+965 86151436</td>
                                    <td>tummy@gmail.com</td>
                                    <td>127.0.10.15</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /> </td>
                                    <td>17-02-2021 6:40:11 AM</td>
                                    <td><a href="#" data-toggle="modal" data-target="#profileuser">Misty Moles</a><!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>support user</td>
                                    <td>+965 85254562</td>
                                    <td>moles@gmail.com</td>
                                    <td>127.0.125.125</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /> </td>
                                    <td>12-01-2021 6:40:11 AM</td>
                                    <td><a href="#" data-toggle="modal" data-target="#profileuser">Billy Hills</a><!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>support user</td>
                                    <td>+965 97252324</td>
                                    <td>hills@gmail.com</td>
                                    <td>127.0.101.1</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /> </td>
                                    <td>24-09-2020 6:40:11 AM</td>
                                    <td><a href="#" data-toggle="modal" data-target="#profileuser">Alex</a><!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>support user</td>
                                    <td>+965 99652532</td>
                                    <td>alex@gmail.com</td>
                                    <td>127.0.255.1</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /> </td>
                                    <td>10-01-2021 6:40:11 AM</td>
                                    <td><a href="#" data-toggle="modal" data-target="#profileuser">Rolodex Propaganda</a><!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>support user</td>
                                    <td>+965 45623582</td>
                                    <td>rolodex@gmail.com</td>
                                    <td>127.0.0.201</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /> </td>
                                    <td>13-01-2021 6:40:11 AM</td>
                                    <td><a href="#" data-toggle="modal" data-target="#profileuser">Micky</a><!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>support user</td>
                                    <td>+965 66134568</td>
                                    <td>micky@gmail.com</td>
                                    <td>127.0.2.200</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /> </td>
                                    <td>31-01-2021 6:40:11 AM</td>
                                    <td><a href="#" data-toggle="modal" data-target="#profileuser">David</a><!-- <a class="cameraicon" ><i class="fa fa-ellipsis-h"></i></a> --></td>
                                    <td>support user</td>
                                    <td>+965 62475869</td>
                                    <td>david@gmail.com</td>
                                    <td>127.0.0.65</td>
                                </tr>
                                                           
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Datatable-->
</div>
<!--Content Area-->
<!-- profile user model -->
<div class="modal fade right" id="profileuser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" class="modal fade"
    aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
      <div class="modal-content">
        <!--Header-->
        <div class="modal-header">
               <button type="button" data-dismiss="modal" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    
                      <h4 id="smallModalLabel" class="modal-title"> Ronald Gamer <!-- &nbsp;<a href="#" data-toggle="modal" data-target="#resetpassword"> <i class="fa fa-key" aria-hidden="true">&nbsp;</i>Reset Password</a> &nbsp;
                      <a href="#" data-toggle="modal" data-target="#" > <i class="fa fa-ban" aria-hidden="true"> &nbsp;</i>Suspend a user </a> &nbsp;
                      <a href="#" data-toggle="modal" data-target="#"> <i class="fa fa-user-times" aria-hidden="true"> &nbsp;</i>Delete users </a> &nbsp; --></h4>
                    
                      <h5 class="internaluser">Support user</h5>
                      <div class="l-spaced">
          <div class="l-row mt-10 l-spaced-bottom">
          <div class="l-col-md-12 text-right">
            <div class="btn-group">
                <a href="#" data-toggle="modal" data-target="#resetpassword" class="btn btn-dark"><i class="fa fa-key" aria-hidden="true"> &nbsp;</i>Reset Password</a>
            </div>
            <div class="btn-group">
               <a href="#" class="btn btn-dark"><i class="fa fa-ban"> &nbsp;</i></i> Suspend a users</a> &nbsp;
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-danger"><i class="fa fa-user-times" aria-hidden="true"> &nbsp;</i>Delete users</button>
            </div>
        </div>
           
        </div>
    </div>
 
                 
        </div>

        <!--Body-->
        <div class="modal-body">
            <section>
      
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item waves-effect waves-light active" >
                  <a class="nav-link" id="account-tab" data-toggle="tab" href="#account" role="tab" aria-controls="account" aria-selected="false">Account</a>
                </li>
                <li class="nav-item waves-effect waves-light">
                  <a class="nav-link" id="devices-tab" data-toggle="tab" href="#devices" role="tab" aria-controls="devices" aria-selected="false">Devices</a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade active in" id="account" role="tabpanel" aria-labelledby="account-tab ">
                 <br>
                 <div class="row">
                 <div class="col-xs-6">
                      <th><b>Username and email</b></th><br>
                      <th>gamer@gmail.com</th><br>
                      <th><a href="#" data-toggle="modal" data-target="#manageusername">Manage username and email</a></th>
                 </div>
                
                 <div class="col-xs-6">
                      <th><b>Last sign-in</b></th><br>
                      <th>No attempts in last 30 days </th><br>
                      <th><a href="#" data-toggle="modal" data-target="#lastsignin">View last sign-in </a></th>
                 </div>
               </div>
               <br>
                 <div class="row">
                   <div class="col-xs-6">
                      <th><b>Roles & Access</b></th><br>
                      <th>Support Ticket</th><br>
                      <th>Support User Creation</th><br>
                      <th><a href="#" data-toggle="modal" data-target="#managerole">Manage roles & access</a></th>
                 </div>
                      <div class="col-xs-6">
                          <th><b>Manager</b></th><br>
                          <th>Ahmed assad</th><br>
                          <th><a href="#" data-toggle="modal" data-target="#addmanager">Add Managar</a></th>
                       </div>
                      </div> 
                      <br>
                      <div class="row">
                        <div class="col-xs-6">
                              <th><b>Contact information</b></th><br>
                              <th>Full Name</th><br>
                              <th>Alex gamer</th><br>
                              <th>Phone Number</th><br>
                              <th>(+965)69564523</th><br>
                              <th><a href="#" data-toggle="modal" data-target="#contactinformation">Manage contact information</a></th>
                        </div>
                        <div class="col-xs-6">
                                <br>
                               <th>eMail Address</th><br>
                               <th>gamer@gmail.com</th><br>  
                        </div>
                      </div>  
                    </div>
                <div class="tab-pane fade" id="devices" role="tabpanel" aria-labelledby="devices-tab">
                 <table class="table">
                   <thead>
                        <tr>
                          <th scope="col">Device</th>
                          <th scope="col">Access Date</th>
                          <th scope="col">IP Address</th>
                          <th scope="col">Browser</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td>Mac Book Air</td>
                          <td>12-01-2021 13:39:27 PM</td>
                          <td>123.345.235.23</td>
                          <td>Safari</td>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr>
                          <td>Mac Mini</td>
                          <td>28-01-2021</td>
                          <td>123.325.255.25</td>
                          <td>Safari</td>
                        </tr>
                    </tbody>
                </table>
            </div>
          </div>
          </section>     
        </div>
        
      </div>
    </div>
  </div>
  </div>
  
  <!-- Modal: invite user -->
  <div class="modal fade right" id="inviteuser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" class="modal fade"
    aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
      <div class="modal-content">
        <!--Header-->
        <div class="modal-header">
            <button type="button" data-dismiss="modal" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 id="smallModalLabel" class="modal-title">INVITE USER</h4>
        </div>

        <!--Body-->
         <div class="modal-body">
            <div>
            <input  name="inviteuser" type="radio"  value="" >
            <label class="form-check-label">Invite Through SMS</label>&nbsp;
            <input  name="inviteuser" type="radio"  value="">
            <label class="form-check-label" >Invite Through Email</label>
            </div>
          <!-- Radio -->
         <br>
          <div>
             <select name="phone number">
                <option value="(+965) Kuwait">(+965) Kuwait</option>
             </select>
          </div>
          <br>
          <!-- textarea-->
          <div class="md-form">
            <textarea type="text"  class="md-textarea form-control" placeholder="Enter Number with comma separator.Please don't add a country code." row="5"></textarea>
          </div>
          <br>
           or 
           <div>
           <th><i class="fa fa-file" aria-hidden="true"></i> import via CSV</th>&nbsp;<a>click here to dowload format</a>
            <br>
            <th>File Name: <a href="C:\Users\XPOSS_PC\Desktop\sample.csv\" target="_blank"><b>filename.csv</b></a>&nbsp;<i class="fa fa-times" aria-hidden="true"></i></th>
           </div>
        </div>
        <!--Footer-->
        <div class="modal-footer justify-content-center">
          <a type="button" class="btn btn-white">validate & invite
            
          </a>
         
        </div>-->
      </div>
    </div> 
  </div>
  
  <!-- Modal: manageusername -->


  
  <div class="modal fade right" id="manageusername" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" class="modal fade"
    aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
      <div class="modal-content">
        <!--Header-->
        <div class="modal-header">
            <button type="button" data-dismiss="modal" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 id="smallModalLabel" class="modal-title">MANAGE USERNAME AND EMAIL</h4>
                <br>
                <p>if the primary email is also their username then changing the primary email will also change their current username</p>
        </div>

        <!--Body-->
         <div class="modal-body">
              <table class="table">
               <tbody>
                    <tr>
                      <td>Primary email address and username</td>
                    </tr>               
                </tbody>
                 <tbody>
                    <tr>
                      <td>gamer@gmail.com</td>
                    </tr>               
                </tbody>
                <tbody>
                    <tr>
                      
                    </tr>               
                </tbody>
            </table>
             <br>
              <div class="form-group">
                   <div class="border border-light p-3 mb-4">
                  <input type="text" class="form-control" id="create-offers" placeholder="please enter email address">
                </div>
                       <!--  <div class="text-center" align="center">
                           <input type="text" class="email2" placeholder="please enter email address" >
                        </div> -->
                    </div>
                </div>       
                  <div class="border border-light p-3 mb-4">
                        <div class="text-center">
                            <button type="button" class="mb-5 btn btn-lg btn-info btn-eff btn-eff-2"><span>Save changes</span></button>
                        </div>
                   </div>
            
          
         </div>
        </div>
      </div>
    </div> 
  </div>
  <!-- Modal: end manageusername -->

  <!-- Modal: lastsignin -->

  <div class="modal fade right" id="lastsignin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" class="modal fade"
    aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
      <div class="modal-content">
        <!--Header-->
        <div class="modal-header">
            <button type="button" data-dismiss="modal" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 id="smallModalLabel" class="modal-title">LAST SIGN-IN</h4>
                <p>This is the last time this user signed in with their username and password. it doesn't include other types of sign-ins,like service to service authentication </p>
        </div>

        <!--Body-->
         <div class="modal-body">
            <table class="table">
              <thead>
                    <tr>
                      <th scope="col">Date</th>
                      <th scope="col">Status</th>
                      <th scope="col">Failure Reason</th>
                    </tr>
                </thead>
               <tbody>
                    <tr>
                      <td>January29,2021,10:34 Am</td>
                      <td>Success</td>
                      <td></td>
                    </tr>
                </tbody>
            </table>
            <a href="#">Click here to dowload full report</a>
        </div>
      </div>
    </div> 
  </div>
  <!-- Modal:  end lastsignin -->
  
    <!-- Modal: managerole -->

  <div class="modal fade right" id="managerole" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" class="modal fade"
    aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
      <div class="modal-content">
        <!--Header-->
        <div class="modal-header">
            <button type="button" data-dismiss="modal" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 id="smallModalLabel" class="modal-title">MANAGE ROLES</h4><br>
                <p>Roles give users permission to view data and complete tasks in backend centres. Give users only the access they need by assigning the least-permissive role</p> 
        </div>

        <!--Body-->
         <div class="modal-body">
            <form>
                <div class="row">
               <div class="form-check">
                 &nbsp; <input class="form-check-input" type="radio" name="roles_permission" id="roles_permission">
                  <label class="form-check-label" for="roles_permission">
                    &nbsp;Support Panel Users
                  </label>
                </div>
                  </div>
                   <div id="role">  
                      <input class="form-check-input" type="checkbox" value="" id="supportticket">
                      &nbsp;<label class="form-check-label" for="supportticket">
                        Create support ticket
                      </label>
                    </div>    
                    <div id="role">  
                      <input class="form-check-input" type="checkbox" value="" id="supportticket">
                      &nbsp;<label class="form-check-label" for="supportticket">
                        Assign support ticket to user
                      </label>
                    </div>
                    <div id="role">  
                      <input class="form-check-input" type="checkbox" value="" id="supportticket">
                      <label class="form-check-label" for="supportticket">
                        Close support ticket
                      </label>
                    </div>
                    <div id="role">  
                      <input class="form-check-input" type="checkbox" value="" id="supportticket">
                      &nbsp;<label class="form-check-label" for="supportticket">
                        Create support user
                      </label>
                    </div>
                    <div id="role">  
                      <input class="form-check-input" type="checkbox" value="" id="supportticket">
                      &nbsp;<label class="form-check-label" for="supportticket">
                        Able to see client information
                      </label>
                    </div>
                    <div id="role">  
                      <input class="form-check-input" type="checkbox" value="" id="supportticket">
                     &nbsp; <label class="form-check-label" for="supportticket">
                        Able to reassign support
                      </label>
                    </div>
                    <div class="row">
               <div class="form-check">
                 &nbsp; <input class="form-check-input" type="radio" name="roles_permission" id="roles_permission">
                  <label class="form-check-label" for="roles_permission">
                    &nbsp;Sub-ordinate user management 
                  </label>
                </div>
                  </div>
                   <div id="role">  
                      <input class="form-check-input" type="checkbox" value="" id="supportticket">
                      &nbsp;<label class="form-check-label" for="supportticket">
                        Able to create user
                      </label>
                    </div>    
                    <div id="role">  
                      <input class="form-check-input" type="checkbox" value="" id="supportticket">
                      &nbsp;<label class="form-check-label" for="supportticket">
                        Assign support ticket to user
                      </label>
                    </div>
                    <div id="role">  
                      <input class="form-check-input" type="checkbox" value="" id="supportticket">
                      <label class="form-check-label" for="supportticket">
                        Close support ticket
                      </label>
                    </div>
                    <div id="role">  
                      <input class="form-check-input" type="checkbox" value="" id="supportticket">
                      &nbsp;<label class="form-check-label" for="supportticket">
                        Create support user
                      </label>
                    </div>
                    <div id="role">  
                      <input class="form-check-input" type="checkbox" value="" id="supportticket">
                      &nbsp;<label class="form-check-label" for="supportticket">
                        Able to see client information
                      </label>
                    </div>
                    <div id="role">  
                      <input class="form-check-input" type="checkbox" value="" id="supportticket">
                     &nbsp; <label class="form-check-label" for="supportticket">
                        Able to reassign support
                      </label>
                    </div>
                     <div class="border border-light p-3 mb-4">
                        <div class="text-center">
                            <button type="button" class="btn btn-white">Save Changes</button>
                        </div>
                </div>
               </form>     

        </div>
      </div>
    </div> 
  </div>
  <!-- Modal:end managerole -->
  
  <!-- Modal: addmanagar -->

  <div class="modal fade right" id="addmanager" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" class="modal fade"
    aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
      <div class="modal-content">
        <!--Header-->
        <div class="modal-header">
            <button type="button" data-dismiss="modal" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 id="smallModalLabel" class="modal-title">ADD MANAGER</h4>
                <p>when you add a manager,will be able to show organizational charts to users,Note that each user can only have one managar at a time.</p> 
        </div>

        <!--Body-->
         <div class="modal-body">
            <form>
                <div class="form-group">
                   <div class="border border-light p-3 mb-4">
                        <div class="text-center" align="center">
                          <label for="switchmanagar">Add, switch, or remove </label>
                         </div>
                    </div>
                 </div>        
              <div class="form-group">
                   <div class="border border-light p-3 mb-4">

                        <div class="text-center" align="center">
                             <input type="text" class="form-control" id="create-offers" placeholder="please add managar">
                           <!-- <input type="text" class="email2" id="switchmanagar"> -->
                        </div>
                    </div>
                </div>       
              <div class="border border-light p-3 mb-4">
                        <div class="text-center">
                            <button type="button" class="btn btn-white">Save Changes</button>
                        </div>
                </div>
            </form>     

        </div>
      </div>
    </div> 
  </div>
  <!-- Modal:  end addmanagar -->
  <!-- Modal: resetpassword -->

  <div class="modal fade right" id="resetpassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" class="modal fade"
    aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
      <div class="modal-content">
        <!--Header-->
        <div class="modal-header">
            <button type="button" data-dismiss="modal" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 id="smallModalLabel" class="modal-title">RESET PASSWORD</h4>
                 
        </div>

        <!--Body-->
         <div class="modal-body">
            <form>
              <div class="form-group">
                    <label for="newpassword">New Password</label>
                    <input type="password" name="password" class="form-control" id="newpassword">
              </div>
              <div class="form-group">
                <label for="confirmpassword">Confirm Password</label>
                <input type="password" name="confirmpassword" class="form-control" id="confirmpassword">
              </div>
                    <div class="border border-light p-3 mb-4">
                        <div class="text-center">
                           <button type="button" class="mb-5 btn btn-lg btn-info btn-eff btn-eff-2"><span>Save changes</span></button>
                        </div>
                    </div>    
            </form>
        </div>
      </div>
    </div> 
  </div>
  <!-- Modal: end resetpassword -->


  <!-- Modal: contactinformation -->

  <div class="modal fade right" id="contactinformation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" class="modal fade"
    aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
      <div class="modal-content">
        <!--Header-->
        <div class="modal-header">
            <button type="button" data-dismiss="modal" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 id="smallModalLabel" class="modal-title">MANAGE CONTACT INFORMATION</h4>
                 
        </div>

        <!--Body-->
         <div class="modal-body">
            <form>
              <div class="form-group">
                    <label for="firstname">First Name</label>
                    <input type="text" class="form-control" id="firstname">
              </div>
              <div class="form-group">
                <label for="lastname">Last Name</label>
                <input type="text" class="form-control" id="lastname">
              </div>
              <div class="form-group">
                <label for="displayname">Display Name</label>
                <input type="text" class="form-control" id="displayname">
              </div>
              <div class="form-group">
                <label for="jobtitle">Job title</label>
                <input type="text" class="form-control" id="jobtitle">
              </div>
              <div class="form-group">
                <label for="department">Department</label>
                <input type="text" class="form-control" id="department">
              </div>
              <div class="form-group">
                <label for="office">Office</label>
                <input type="text" class="form-control" id="office">
              </div>
              <div class="form-group">
                  <div class="col-xs-6">
                    <label for="officephone">Office phone</label>
                    <input type="text" class="form-control" id="officephone">
                  </div>
                </div>  
                  <div class="form-group row">
                      <div class="col-xs-6">
                        <label for="mobilephone">Mobile phone</label>
                        <input type="text" class="form-control" id="mobilephone">
                      </div>
                    </div>
                    <div class="border border-light p-3 mb-4">
                        <div class="text-center">
                            <button type="button" class="btn btn-white">Save Changes</button>
                        </div>
                    </div>    
            </form>
        </div>
      </div>
    </div> 
  </div>
  <!-- Modal: end contactinformation -->
<!-- jQuery-->
<script src="{{ asset('assets/js/basic/jquery.min.js')}} "></script>
<script src="{{ asset('assets/js/basic/jquery-migrate.min.js')}}"></script>
<script src="{{ asset('assets/js/calls/part.header.1.js')}}"></script>
<script src="{{ asset('assets/js/calls/part.sidebar.2.js')}}"></script>
<script src="{{ asset('assets/js/calls/part.theme.setting.js')}}"></script>
<script src="{{ asset('assets/js/calls/table.data.js')}}"></script>
<script src="{{ asset('assets/js/plugins/table/dataTables.autoFill.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/table/dataTables.colReorder.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/table/dataTables.colVis.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/table/dataTables.responsive.min.js')}}"></script>
<!-- General-->
<script src="{{ asset('assets/js/basic/modernizr.min.js')}}"></script>
<script src="{{ asset('assets/js/basic/bootstrap.min.js')}}"></script>
<script src="{{ asset('assets/js/shared/jquery.asonWidget.js')}}"></script>
<script src="{{ asset('assets/js/plugins/plugins.js')}}"></script>
<script src="{{ asset('assets/js/general.js')}}"></script>

<script src="{{ asset('assets/js/plugins/pageprogressbar/pace.min.js')}}"></script>
<!-- Specific-->
<script src="{{ asset('assets/js/shared/classie.js')}}"></script>
<script src="{{ asset('assets/js/shared/jquery.cookie.min.js')}}"></script>
<script src="{{ asset('assets/js/shared/moment.min.js')}}"></script>
<script src="{{ asset('assets/js/shared/perfect-scrollbar.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/accordions/jquery.collapsible.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/datetime/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/datetime/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/datetime/clockface.js')}}"></script>
<script src="{{ asset('assets/js/plugins/datetime/daterangepicker.js')}}"></script>
<script src="{{ asset('assets/js/plugins/datetime/jqClock.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/forms/elements/jquery.checkBo.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/forms/elements/jquery.switchery.min.j')}}s"></script>
<script src="{{ asset('assets/js/plugins/notifications/jquery.jgrowl.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/tooltip/jquery.tooltipster.min.js')}}"></script>
<script src="{{ asset('assets/js/calls/part.header.1.js')}}"></script>
<script src="{{ asset('assets/js/calls/part.sidebar.2.js')}}"></script>
<script src="{{ asset('assets/js/calls/part.theme.setting.js')}}"></script>
<script src="{{ asset('assets/js/calls/ui.datetime.js')}}"></script>
<script type="text/javascript">
      $("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
});
    </script>
@endsection
