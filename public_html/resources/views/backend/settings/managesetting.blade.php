@extends('backends_layouts.master',['title'=>'Setting Management'])
@section('content')
<div class="l-page-header">
        <h2 class="l-page-title"><span>Setting</span></h2>
        <!--BREADCRUMB-->
        <ul class="breadcrumb t-breadcrumb-page">
            <li><a href="dashboard.php">Home</a></li>
            <li class="active">Setting</li>
        </ul>
        <!--END BREADCRUMB-->
    </div>
    <!--Content Area-->
    <div class="l-spaced l-box l-spaced-bottom">
        <div class="l-box-body l-spaced">
            <div class="l-row">
                <div class="l-row">
                    <div class="l-col-lg-6 l-col-md-12">
                        <div class="1-row">
                            <h4><b>Application Setting</b></h4>
                            <div class="l-row mt-10">
                                <div data-provides="fileinput" class="fileinput fileinput-new">
                                    <div style="width: 120px; height: 120px;" class="fileinput-new thumbnail"><img src="img/profile/profile.jpg"></div>
                                    <div style="max-width: 120px; max-height: 120px;" class="fileinput-preview fileinput-exists thumbnail"></div>
                                    <div> <span class="btn btn-dark btn-file"><span class="fileinput-new">Select Site Logo</span><span class="fileinput-exists">Change</span>
                                        <input type="file" name="...">
                                        </span><a href="#" data-dismiss="fileinput" class="btn btn-default fileinput-exists">Remove</a> </div>
                                </div>
                            </div>
                            <h5><b>English setting</b></h5>
                            <div class="l-row">
                                <input type="text" placeholder="Please enter application name" class="l-spaced-bottom form-control"> 
                            </div>
                            <div class="l-row mt-10">
                                <input type="text" placeholder="Please enter sub site or tag line" class="l-spaced-bottom form-control"> 
                            </div>
                            <div class="l-row mt-10">
                                <input type="text" placeholder="Adress" class="l-spaced-bottom form-control"> 
                            </div>
                            <div class="l-row mt-10">
                                <input type="text" placeholder="Contact" class="l-spaced-bottom form-control"> 
                            </div>
                            <div class="l-row mt-10">
                                <input type="text" placeholder="Email" class="l-spaced-bottom form-control"> 
                            </div>
                            <div class="l-row text-right">
                                <h5><b>Arabic Content</b></h5>
                            </div>
                            <div class="l-row mt-10">
                                <input type="text" placeholder="Please enter application name" class="l-spaced-bottom form-control text-right"> </div>
                            <div class="l-row mt-10">
                                <input type="text" placeholder="Please enter sub site or tag line" class="l-spaced-bottom form-control text-right"> </div>
                            <div class="l-row mt-10">
                                <input type="text" placeholder="Adress" class="l-spaced-bottom form-control text-right"> </div>
                            <div class="l-row mt-10">
                                <input type="text" placeholder="Contact" class="l-spaced-bottom form-control text-right"> </div>
                            <div class="l-row mt-10">
                                <input type="text" placeholder="Email" class="l-spaced-bottom form-control text-right"> </div>
                            <div class="l-row mt-10">
                                <div class="l-col-md-12">
                                    <div class="l-col-md-6">
                                        <input type="text" placeholder="Longitude" class="l-spaced-bottom form-control"> </div>
                                    <div class="l-col-md-6">
                                        <input type="text" placeholder="Longitude" class="l-spaced-bottom form-control"> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="l-col-lg-6 l-col-md-12">
                        <div class="1-row">
                            <h4><b>Maintenance & App version</b></h4>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1"> &nbsp; &nbsp; Activate Maintenance Mode</label>
                            </div>
                            <h5><b>English Content</b></h5>
                            <div class="l-row mt-10">
                                <textarea type="textarea" placeholder="Message For Maintenance Mode/Force Update" rows="4" class="l-spaced-bottom form-control"></textarea>
                            </div>
                            <div class="l-row text-right">
                                <h5><b>Arabic Content</b></h5>
                            </div>
                            <div class="l-row mt-10">
                                <textarea type="textarea" placeholder="Message For Maintenance Mode/Force Update" rows="4" class="l-spaced-bottom form-control text-right"></textarea>
                            </div>
                            <h5><b>Arabic Version Management</b></h5>
                            <div class="l-row mt-10">
                                <div class="l-col-md-12">
                                    <div class="l-col-md-6">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1"> &nbsp; &nbsp; Activate Android Force Update</label>
                                        </div>
                                    </div>
                                    <div class="l-col-md-6">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1"> &nbsp; &nbsp; Activate iPhone Force Update</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="l-row mt-10">
                                <div class="l-col-md-12">
                                    <div class="l-col-md-6">
                                        <input type="text" placeholder="Android Application Version" class="l-spaced-bottom form-control"> 
                                    </div>
                                    <div class="l-col-md-6">
                                        <input type="text" placeholder="iPhone Application Version" class="l-spaced-bottom form-control"> 
                                    </div>
                                </div>
                            </div>
                            <h5><b>THIRD PARTY KET SETUP</b></h5>
                            <div class="l-row mt-10">
                                <input type="text" placeholder="Google map key" class="l-spaced-bottom form-control"> 
                            </div>
                            <div class="l-row mt-10">
                                <input type="text" placeholder="Mapbox key" class="l-spaced-bottom form-control"> 
                            </div>
                            <div class="l-row mt-10">
                                <input type="text" placeholder="Paci IT map key" class="l-spaced-bottom form-control"> 
                            </div>
                            <div class="l-row mt-10">
                                <input type="text" placeholder="Firebase Notification   key" class="l-spaced-bottom form-control"> 
                            </div>
                            <div class="l-row mt-10">
                                <input type="text" placeholder="Email Services key" class="l-spaced-bottom form-control"> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="l-col-md-12 mt-10">
                    <div class="fr">
                        <button type="button" class="mb-5 btn btn-lg btn-info btn-eff btn-eff-2"><span>Update</span></button>
                        <a href="dashboard.php">
                            <button type="button" class="mb-5 btn btn-lg btn-dark btn-eff btn-eff-2"><span>Cancel</span></button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection