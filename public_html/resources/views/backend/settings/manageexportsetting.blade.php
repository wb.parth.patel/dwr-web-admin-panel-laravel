@extends('backends_layouts.master',['title'=>'Explorer Pages setting Management'])
@section('content')
<link rel="stylesheet" href="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1569006288/BBBootstrap/choices.min.css?version=7.0.0">
<script src="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1569006273/BBBootstrap/choices.min.js?version=7.0.0"></script>

<link href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'>
<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>

<!--Content Area-->
<div class="l-page-header">
    <h2 class="l-page-title"><span>Explorer Page Management</span></h2>
    <div class="fr">
    </div>
    <!--BREADCRUMB-->
    <ul class="breadcrumb t-breadcrumb-page">
        <li><a href="dashboard.php">Home</a></li>
        <li class="active">Explorer Page Management</li>
    </ul>
    <!--END BREADCRUMB-->
</div>
<div class="l-spaced">
    <h4 class="l-page-title"><span></span></h4>
    <div class="l-row mt-10 l-spaced-bottom">
        <div class="l-col-md-12 text-right">
            <!-- <div class="btn-group">
                <a href="#" data-toggle="modal" data-target="#Modal-Add-package" class="btn btn-dark"><i class="fa fa-plus"> &nbsp;</i> Create package</a>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-success dropdown-toggle"><i class="fa fa-refresh"> &nbsp;</i>Refresh</button>
            </div>
            <div class="btn-group">
                <button type="button" data-toggle="modal" data-target="#smallModalExport" class="btn btn-info dropdown-toggle"><i class="fa fa-download"> &nbsp;</i> Export</button>
            </div> -->
        </div>
    </div>
    <!--Datatable-->
    <div id="tables" class="resp-tabs-skin-1">
        <div class="resp-tabs-container">
            <!-- Default Table-->
            <div class="l-row l-spaced-bottom">
                <div class="l-box">
                    <div class="l-box-body">
                    <div class="l-spaced-bottom">
                    <div class="l-box-body l-spaced">
                        <div class="l-row">
                            <div class="l-row">
                                <div class="l-col-lg-12 l-col-md-12">
                                    <div class="1-row">
                                        <h4><b>Category</b></h4>
                                      
                                        <select id="choices-multiple-remove-button" placeholder="Select Category" multiple>
                                            <option value="Hotel">Hotel</option>
                                            <option value="petrol">petrol</option>
                                            <option value="Atm">Atm</option>
                                            <option value="Medical">Medical</option>
                                            <option value="Bank">Bank</option>
                                           
                                        </select> 
                                        <!-- End -->
                                    </div><hr>
                                    <div class="1-row">
                                        <h4><b>Top Pic Setting</b></h4>
                                        <div class="form-check">
                                        
                                            <select id="TopPicSetting" placeholder="Select Top Pic Setting" multiple>
                                            <option value="Restaurant1">Restaurant 1</option>
                                            <option value="Restaurant 2">Restaurant 2</option>
                                            <option value="Restaurant 3">Restaurant 3</option>
                                            <option value="Restaurant 4">Restaurant 4</option>
                                            <option value="Restaurant 5">Restaurant 5</option>
                                            <option value="Restaurant6">Restaurant 6</option>
                                            <option value="Restaurant 7">Restaurant 7</option>
                                            <option value="Restaurant 8">Restaurant 8</option>
                                            <option value="Restaurant 9">Restaurant 9</option>
                                            <option value="Restaurant 10">Restaurant 10</option>
                                          
                                        </select> 
                                        </div>
                                    </div><hr>
                                    <div class="1-row">
                                        <h4><b>Offer</b></h4>
                                        <div class="form-check">
                                            
                                            <select id="offer" placeholder="Select Offer" multiple>
                                            <option value="offer 1">offer 1</option>
                                            <option value="offer 2">offer 2</option>
                                            <option value="offer 3">offer 3</option>
                                            <option value="offer 4">offer 4</option>
                                            <option value="offer 5">offer 5</option>
                                            <option value="offer 6">offer 6</option>
                                            <option value="offer 7">offer 7</option>
                                            <option value="offer 8">offer 8</option>
                                            <option value="offer 9">offer 9</option>
                                            <option value="offer 10">offer 10</option>
                                         
                                        </select> 
                                        </div>
                                    </div><hr>
                                    <div class="1-row">
                                        <h4><b>Event</b></h4>
                                        <div class="form-check">
                                        
                                            <select id="event" placeholder="Select event" multiple>
                                            <option value="event 1">event 1</option>
                                            <option value="event 2">event 2</option>
                                            <option value="event 3">event 3</option>
                                            <option value="event 4">event 4</option>
                                            <option value="event 5">event 5</option>
                                            <option value="event 6">event 6</option>
                                            <option value="event 7">event 7</option>
                                            <option value="event 8">event 8</option>
                                            <option value="event 9">event 9</option>
                                            <option value="event 10">event 10</option>
                                          
                                        </select> 
                                        </div>
                                    </div><hr>
                                    <div class="1-row">
                                        <h4><b>Packages</b></h4>
                                        <div class="form-check">
                                          
                                            <select id="Packages" placeholder="Select packages" multiple>
                                            <option value="Packages 1">Packages 1</option>
                                            <option value="Packages 2">Packages 2</option>
                                            <option value="Packages 3">Packages 3</option>
                                            <option value="Packages 4">Packages 4</option>
                                            <option value="Packages 5">Packages 5</option>
                                            <option value="Packages 6">Packages 6</option>
                                            <option value="Packages 7">Packages 7</option>
                                            <option value="Packages 8">Packages 8</option>
                                            <option value="Packages 9">Packages 9</option>
                                            <option value="Packages 10">Packages 10</option>
                                           
                                        </select> 
                                        </div>
                                    </div><hr>
                                    <div class="1-row">
                                        <h4><b>Advertisement</b></h4>
                                        <div class="form-check">
                                         
                                            <select id="Advertisement" placeholder="Select advertisement" multiple>
                                            <option value="Advertisement 1">Advertisement 1</option>
                                            <option value="Advertisement 2">Advertisement 2</option>
                                            <option value="Advertisement 3">Advertisement 3</option>
                                            <option value="Advertisement 4">Advertisement 4</option>
                                            <option value="Advertisement 5">Advertisement 5</option>
                                            <option value="Advertisement 6">Advertisement 6</option>
                                            <option value="Advertisement 7">Advertisement 7</option>
                                            <option value="Advertisement 8">Advertisement 8</option>
                                            <option value="Advertisement 9">Advertisement 9</option>
                                            <option value="Advertisement 10">Advertisement 10</option>
                                          
                                        </select> 
                                        </div>
                                    </div><hr>
                            </div>
                            <div class="l-col-md-12 mt-10">
                                <div class="fr">
                                    <button type="button" class="mb-5 btn btn-lg btn-info btn-eff btn-eff-2"><span>Save</span></button>
                                    <button type="button" class="mb-5 btn btn-lg btn-dark btn-eff btn-eff-2" data-dismiss="modal"><span>Cancel</span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Datatable-->
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var multipleCancelButton = new Choices('#choices-multiple-remove-button', {
        removeItemButton: true,
        searchResultLimit:5,
        renderChoiceLimit:5
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var multipleCancelButton = new Choices('#TopPicSetting', {
        removeItemButton: true,
        searchResultLimit:5,
        renderChoiceLimit:5
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var multipleCancelButton = new Choices('#offer', {
        removeItemButton: true,
        searchResultLimit:5,
        renderChoiceLimit:5
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var multipleCancelButton = new Choices('#event', {
        removeItemButton: true,
        searchResultLimit:5,
        renderChoiceLimit:5
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var multipleCancelButton = new Choices('#Packages', {
        removeItemButton: true,
        searchResultLimit:5,
        renderChoiceLimit:5
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var multipleCancelButton = new Choices('#Advertisement', {
        removeItemButton: true,
        searchResultLimit:5,
        renderChoiceLimit:5
        });
    });
</script>
<!-- jQuery-->
<!-- <script src="js/basic/jquery.min.js"></script>
<script src="js/basic/jquery-migrate.min.js"></script>
<script src="js/calls/part.header.1.js"></script>
<script src="js/calls/part.sidebar.2.js"></script>
<script src="js/calls/part.theme.setting.js"></script>
<script src="js/calls/table.data.js"></script>
<script src="js/plugins/table/dataTables.autoFill.min.js"></script>
<script src="js/plugins/table/dataTables.colReorder.min.js"></script>
<script src="js/plugins/table/dataTables.colVis.min.js"></script>
<script src="js/plugins/table/dataTables.responsive.min.js"></script> -->
<script type="text/javascript">
         $(function (){
            $('#example-getting-started').multiselect({
                includeSelectAllOption: true,
                enableClickableOptGroups: true
            });
        });
</script>
@endsection