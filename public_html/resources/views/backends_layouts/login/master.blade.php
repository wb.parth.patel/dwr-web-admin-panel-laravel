@include('backends_layouts.login.head')
  <body class="login-bg login-container-bg">
    
    <!--[if lt IE 9]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
    <!--SECTION-->
    <section class="l-main-container">
      @yield('content')

      @include('backends_layouts.login.footer')
    </section>
    @stack('scripts')
  </body>
</html>