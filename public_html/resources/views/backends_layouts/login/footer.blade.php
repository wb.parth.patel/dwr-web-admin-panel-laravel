<!-- ===== JS =====-->
    <!-- jQuery-->
    <script src="{{ asset('assets/js/basic/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/js/basic/jquery-migrate.min.js')}}"></script>
    <!-- General-->
    <script src="{{ asset('assets/js/basic/modernizr.min.js')}}"></script>
    <script src="{{ asset('assets/js/basic/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/js/shared/jquery.asonWidget.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/plugins.js')}}"></script>
    <script src="{{ asset('assets/js/general.js')}}"></script>
    <!-- Semi general-->
    <script type="text/javascript">
      var paceSemiGeneral = { restartOnPushState: false };
      if (typeof paceSpecific != 'undefined'){
      	var paceOptions = $.extend( {}, paceSemiGeneral, paceSpecific );
      	paceOptions = paceOptions;
      }else{
      	paceOptions = paceSemiGeneral;
      }
      
    </script>
    <script src="{{ asset('assets/js/plugins/pageprogressbar/pace.min.js')}}"></script>
    <!-- Specific-->
    <script src="{{ asset('assets/js/plugins/forms/validation/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/forms/validation/jquery.validate.additional.min.js')}}"></script>
    <script src="{{ asset('assets/js/calls/page.login.js')}}"></script>