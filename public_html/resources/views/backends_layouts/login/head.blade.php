<!DOCTYPE html>
<html lang="en">
  <head>
    <title>DrawApp MANAGEMENT PORTAL - Login</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.ico')}}">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="{{ asset('assets/css/basic.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/general.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/theme.css')}}" class="style-theme">
    
    <!--[if lt IE 9]>
    <script src="js/basic/respond.min.js"></script>
    <script src="js/basic/html5shiv.min.js"></script>
    <![endif]-->
  </head>