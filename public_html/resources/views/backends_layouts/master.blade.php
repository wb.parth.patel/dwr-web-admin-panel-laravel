<!DOCTYPE html>
<html lang="en">
<head>
    @include('backends_layouts.head')
</head>
<body class="l-footer-sticky-1 l-dashboard">

    <!--[if lt IE 9]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <!--SECTION-->
    <section class="l-main-container has-footer-1">
        <!--Left Sidebar Content--> 
        @include('backends_layouts.sidebar')
        <!--END Left Sidebar Content-->
        <!--Main Content-->
        <section class="l-container">
            <!--HEADER-->
            @include('backends_layouts.header-bar')

            @yield('content')

            @include('backends_layouts.footer')

            @stack('scripts')
</body>
</html>