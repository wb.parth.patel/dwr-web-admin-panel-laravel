            <!--FOOTER-->
            <footer class="l-footer l-footer-1 t-footer-1">
                <div class="group pt-10 pb-10 ph">
                    <div class="copyright pull-left">
                        © Copyright 2021
                        <a href="#">DWR App</a>
                        All rights reserved.
                    </div>
                    <div class="version pull-right">v 1.0</div>
                </div>
            </footer>
    </section><!--Left Sidebar Content-->
</section><!--Main Content-->

    <!-- ===== JS =====-->
    <!-- jQuery-->
    <script src="{{ asset('assets/js/basic/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/js/basic/jquery-migrate.min.js')}}"></script>
    <!-- General-->
    <script src="{{ asset('assets/js/basic/modernizr.min.js')}}"></script>
    <script src="{{ asset('assets/js/basic/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/js/shared/jquery.asonWidget.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/plugins.js')}}"></script>
    <script src="{{ asset('assets/js/general.js')}}"></script>
    <!-- Semi general-->
    <script type="text/javascript">
        var paceSemiGeneral = { restartOnPushState: false };
        if (typeof paceSpecific != 'undefined') {
            var paceOptions = $.extend({}, paceSemiGeneral, paceSpecific);
            paceOptions = paceOptions;
        } else {
            paceOptions = paceSemiGeneral;
        }
        $(document).ready(function () {
            $('#select2Default').select2();
        });
    </script>
    
    <script src="{{ asset('assets/js/shared/jquery.easing.1.3.js')}}"></script>
    <script src="{{ asset('assets/js/shared/perfect-scrollbar.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/accordions/jquery.collapsible.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/charts/c3/c3.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/charts/c3/d3.v3.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/charts/other/jquery.easypiechart.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/charts/rickshaw/rickshaw.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/datetime/jqClock.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/datetime/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/datetime/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="{{ asset('assets/assets/js/plugins/datetime/clockface.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/datetime/daterangepicker.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/forms/elements/jquery.checkBo.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/forms/elements/jquery.switchery.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/table/footable.all.min.js')}} "></script>
    <script src="{{ asset('assets/js/plugins/tabs/jquery.easyResponsiveTabs.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/textrotator/jquery.simple-text-rotator.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/tooltip/jquery.tooltipster.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/weather/jquery.simpleWeather.min.js')}}"></script>
    <script src="{{ asset('assets/js/calls/dashboard.1.js')}}"></script>
    <script src="{{ asset('assets/js/calls/part.header.1.js')}}"></script>
    <script src="{{ asset('assets/js/calls/part.sidebar.2.js')}}"></script>
    <script src="{{ asset('assets/js/calls/part.theme.setting.js')}}"></script>
    <script src="{{ asset('assets/js/calls/shared.tooltipster.js')}}"></script>
    <script src="{{ asset('assets/js/calls/ui.datetime.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/pageprogressbar/pace.min.js')}}"></script>

    <!-- Specific-->
    <script src="{{ asset('assets/js/plugins/forms/elements/jquery.select2.min.js')}}"></script>
    <script src="{{ asset('assets/js/shared/classie.js')}}"></script>
    <script src="{{ asset('assets/js/shared/jquery.cookie.min.js')}}"></script>
    <script src="{{ asset('assets/js/shared/jasny-bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/js/shared/perfect-scrollbar.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/accordions/jquery.collapsible.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/forms/elements/jquery.checkBo.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/forms/elements/jquery.switchery.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/forms/elements/jquery.checkradios.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/table/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/tabs/jquery.easyResponsiveTabs.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/tooltip/jquery.tooltipster.min.js')}}"></script>
    <script src="{{ asset('assets/js/calls/table.data.js')}}"></script>
    <script src="{{ asset('assets/js/calls/page.profile.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/table/dataTables.autoFill.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/table/dataTables.colReorder.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/table/dataTables.colVis.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/table/dataTables.responsive.min.js')}}"></script>

    <!--Content Area-->
                <!-- jQuery-->
                <!-- <script src="{{ asset('assets/js/basic/jquery.min.js')}}"></script>
                <script src="{{ asset('assets/js/basic/jquery-migrate.min.js')}}"></script>
                <script src="{{ asset('assets/js/calls/part.header.1.js')}}"></script>
                <script src="{{ asset('assets/js/calls/part.sidebar.2.js')}}"></script>
                <script src="{{ asset('assets/js/calls/part.theme.setting.js')}}"></script>
                <script src="{{ asset('assets/js/calls/table.data.js')}}"></script>
                <script src="{{ asset('assets/js/plugins/table/dataTables.autoFill.min.js')}}"></script>
                <script src="{{ asset('assets/js/plugins/table/dataTables.colReorder.min.js')}}"></script>
                <script src="{{ asset('assets/js/plugins/table/dataTables.colVis.min.js')}}"></script>
                <script src="{{ asset('assets/js/plugins/table/dataTables.responsive.min.js')}}"></script> -->

                <!-- chart script -->
                <script src="https://code.highcharts.com/highcharts.js"></script>
                <script src="https://code.highcharts.com/modules/exporting.js"></script>
                <script src="https://code.highcharts.com/modules/export-data.js"></script>
                <script src="https://code.highcharts.com/modules/accessibility.js"></script>
                <script>
                Highcharts.chart('container', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'User and Bussiness Listing Trends'
                    },
                    subtitle: {
                        // text: 'Source: WorldClimate.com'
                    },
                    xAxis: {
                        categories: [
                            'Jan',
                            'Feb',
                            'Mar',
                            'Apr',
                            'May',
                            'Jun',
                            'Jul',
                            'Aug',
                            'Sep',
                            'Oct',
                            'Nov',
                            'Dec'
                        ],
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            // text: 'Rainfall (mm)'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{
                        name: 'Register Users',
                        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

                    }, {
                        name: 'Guest Users',
                        data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

                    }, {
                        name: 'Bussiness Users',
                        data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

                    }]  
                });
                </script>
                <script>
                    Highcharts.chart('container2', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Top Categories and Business Search'
                    },
                    subtitle: {
                        // text: 'Source: WorldClimate.com'
                    },
                    xAxis: {
                        categories: [
                            
                        ],
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Rainfall (mm)'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{
                        name: 'Register Users',
                        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

                    }]  
                });
                </script>

                <!-- chart support ticket -->
                <!-- <script src="https://code.highcharts.com/highcharts.js"></script>
                <script src="https://code.highcharts.com/modules/series-label.js"></script>
                <script src="https://code.highcharts.com/modules/exporting.js"></script>
                <script src="https://code.highcharts.com/modules/export-data.js"></script> -->
                <script>
                    Highcharts.chart('container12', {
                    chart: {
                        type: 'spline'
                    },
                    title: {
                        text: 'Support Ticket Management'
                    },
                    subtitle: {
                        // text: 'Source: WorldClimate.com'
                    },
                    xAxis: {
                        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    },
                    yAxis: {
                        title: {
                            // text: 'Temperature'
                        },
                        labels: {
                            formatter: function () {
                                return this.value + '°';
                            }
                        }
                    },
                    tooltip: {
                        crosshairs: true,
                        shared: true
                    },
                    plotOptions: {
                        spline: {
                            marker: {
                                radius: 4,
                                lineColor: '#666666',
                                lineWidth: 1
                            }
                        }
                    },
                    series: [{
                        name: 'Open ticket',
                        data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 23.3, 18.3, 13.9, 9.6]

                    }, {
                        name: 'Close ticket',
                        data: [4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
                    }]
                });
                    </script>
