<aside id="sb-left" class="l-sidebar l-sidebar-1 t-sidebar-1 l-sidebar-left l-initial">
            <!--Switcher-->
            <div class="l-side-box">
                <a href="#" data-ason-type="sidebar" data-ason-to-sm="sidebar" data-ason-target="#sb-left" class="sidebar-switcher switcher t-switcher-side ason-widget"><i class="fa fa-bars"></i></a>
            </div>
            <div class="l-side-box">
                <!--Logo-->
                <div class="widget-logo logo-in-side">
                    <h1>
                        <a href="dashboard.php">
                            <span class="logo-default visible-default-inline-block">
                                <img src="{{ asset('assets/img/logo.png')}}" height="85" alt="DWR">
                            </span>
                            <span class="logo-medium visible-compact-inline-block">
                                <img src="{{ asset('assets/img/logo_medium.png')}}" alt="DWR" title="DWR">
                            </span> 
                        </a>
                    </h1>
                </div>
            </div>
            <!--Main Menu-->
            <div class="l-side-box">
                <!--MAIN NAVIGATION MENU-->
                <nav class="navigation">
                    <ul data-ason-type="menu" class="ason-widget">
                        <li class="<?php if(basename($_SERVER['PHP_SELF'])=='dashboard.php') {echo 'active';}?>">
                                <a href="{{ url('dashboard') }}"><i class="icon fa fa-dashboard"></i>
                                    <span class="title">Dashboard</span>
                                </a>
                        </li>

                        <li class="<?php if(basename($_SERVER['PHP_SELF'])=='support-listing.php' || basename($_SERVER['PHP_SELF'])=='support-tickets-details.php') {echo 'active';}?>">
                                <a href="{{ url('managesupportticket')}}"><i class="icon fa fa-phone"></i>
                                    <span class="title">Manage Support Ticket</span>
                                </a>
                        </li>

                        <li class="<?php if(basename($_SERVER['PHP_SELF'])=='business-management.php') {echo 'active';}?>">
                                <a href="{{ url('managebusiness')}}"><i class="icon fa fa-building"></i>
                                    <span class="title">Manage Business</span>
                                </a>
                        </li>

                        <li class="<?php if(basename($_SERVER['PHP_SELF'])=='application-users-listing.php' || basename($_SERVER['PHP_SELF'])=='internal-user-management.php' || basename($_SERVER['PHP_SELF'])=='user-details.php') {echo 'active';}?>">
                                <a href="#"><i class="icon fa fa-user"></i>
                                    <span class="title">Users</span><span class="arrow"><i class="fa fa-angle-left"></i>
                                        </span>
                                </a>
                            <ul>
                                <li class="<?php if(basename($_SERVER['PHP_SELF'])=='application-users-listing.php' || basename($_SERVER['PHP_SELF'])=='user-details.php') {echo 'active';}?>">
                                        <a href="{{url('manageapplicationuser')}}"><span class="title">Application Users</span>
                                        </a>
                                </li>

                                <li class="<?php if(basename($_SERVER['PHP_SELF'])=='internal-user-management.php') {echo 'active';}?>">
                                    <a href="{{url('managedwruser')}}"><span class="title">DwR Internal Users</span></a>
                                </li>
                            </ul>
                        </li>
                        <!-- <li class="<?php if(basename($_SERVER['PHP_SELF'])=='#' || basename($_SERVER['PHP_SELF'])=='#') {echo 'active';}?>">
                            <a href="#"><i class="icon fa fa-cogs"></i><span class="title">Marketing Tools</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                            <ul>
                                <li class="<?php if(basename($_SERVER['PHP_SELF'])=='#' || basename($_SERVER['PHP_SELF'])=='#') {echo 'active';}?>">
                                    <a href="#"><span class="title">Manage Tools</span></a>
                                </li>
                            </ul>   
                        </li> -->
                        <li class="<?php if(basename($_SERVER['PHP_SELF'])=='setting.php' || basename($_SERVER['PHP_SELF'])=='explorer_pages.php') {echo 'active';}?>">
                            <a href="#"><i class="icon fa fa-cog"></i><span class="title">Settings</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                            <ul>
                                <li class="<?php if(basename($_SERVER['PHP_SELF'])=='setting.php') {echo 'active';}?>">
                                    <a href="{{url('managesettings')}}"><span class="title">Manage Settings</span></a>
                                </li>
                                <li class="<?php if(basename($_SERVER['PHP_SELF'])=='explorer_pages.php') {echo 'active';}?>">
                                        <a href="{{url('manageexplorersettings')}}"><span class="title">Manage Explorer Pages Settings</span></a>
                                </li>
                            </ul>
                        </li>

                        <li class="<?php if(basename($_SERVER['PHP_SELF'])=='area-listing.php' || basename($_SERVER['PHP_SELF'])=='label-listing.php' || basename($_SERVER['PHP_SELF'])=='category-listing.php' || basename($_SERVER['PHP_SELF'])=='package-listing.php' || basename($_SERVER['PHP_SELF'])=='cms-pages.php' || basename($_SERVER['PHP_SELF'])=='amenities-listing.php' || basename($_SERVER['PHP_SELF'])=='offers-listing.php' || basename($_SERVER['PHP_SELF'])=='events-listing.php' || basename($_SERVER['PHP_SELF'])=='location-management.php') {echo 'active';}?>">
                            <a href="#"><i class="icon fa fa-cogs"></i><span class="title">Master</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                            <ul>
                                <li class="<?php if(basename($_SERVER['PHP_SELF'])=='area-listing.php') {echo 'active';}?>">
                                    <a href="area-listing.php"><span class="title">Manage Areas</span></a>
                                </li>
                                <li class="<?php if(basename($_SERVER['PHP_SELF'])=='label-listing.php') {echo 'active';}?>">
                                    <a href="label-listing.php"><span class="title">Manage Label</span></a>
                                </li>
                                <li class="<?php if(basename($_SERVER['PHP_SELF'])=='category-listing.php') {echo 'active';}?>">
                                    <a href="category-listing.php"><span class="title">Manage Category</span></a>
                                </li>
                                <li class="<?php if(basename($_SERVER['PHP_SELF'])=='package-listing.php') {echo 'active';}?>">
                                    <a href="package-listing.php"><span class="title">Manage Package</span></a>
                                </li>
                                <li class="<?php if(basename($_SERVER['PHP_SELF'])=='cms-pages.php') {echo 'active';}?>">
                                    <a href="cms-pages.php"><span class="title">Manage CMS Pages</span></a>
                                </li>
                                <li class="<?php if(basename($_SERVER['PHP_SELF'])=='amenities-listing.php') {echo 'active';}?>">
                                    <a href="amenities-listing.php"><span class="title">Manage Amenities</span></a>
                                </li>
                                <li class="<?php if(basename($_SERVER['PHP_SELF'])=='offers-listing.php') {echo 'active';}?>">
                                    <a href="offers-listing.php"><span class="title">Manage Offers</span></a>
                                </li>
                                <li class="<?php if(basename($_SERVER['PHP_SELF'])=='events-listing.php') {echo 'active';}?>">
                                    <a href="events-listing.php"><span class="title">Manage Events</span></a>
                                </li>
                                <li class="<?php if(basename($_SERVER['PHP_SELF'])=='location-management.php') {echo 'active';}?>">
                                    <a href="location-management.php"><span class="title">Manage Location</span></a>
                                </li> 
                            </ul>
                        </li>

                        <li class="<?php if(basename($_SERVER['PHP_SELF'])=='#') {echo 'active';}?>">
                            <a href="#"><i class="icon fa fa-line-chart"></i><span class="title">Reports</span></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>