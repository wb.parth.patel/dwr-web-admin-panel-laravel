<!DOCTYPE html>
<html lang="en">
<head>
    <title>DWR App Admin | {{$title}}</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.ico')}}">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="{{ asset('assets/css/basic.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/general.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/theme.css')}}" class="style-theme">
    <link rel="stylesheet" href="{{ asset('assets/css/theme.css')}}" class="style-theme">
    <link rel="stylesheet" href="{{ asset('assets/css/addons/theme/summernote.css')}}" class="style-theme-addon"/>
    <!-- Specific-->
    <link rel="stylesheet" href="{{ asset('assets/css/addons/fonts/artill-clean-icons.css')}}"/>
    <link rel="stylesheet" href="{{ asset('assets/css/addons/theme/jasny-bootstrap.css')}}" class="style-theme-addon"/>
    <link rel="stylesheet" href="{{ asset('assets/css/addons/theme/select2.css')}}" class="style-theme-addon" />
    <link rel="stylesheet" href="{{ asset('assets/css/addons/fullcalendar.print.css')}}" media="print">

    <!--[if lt IE 9]>
    <script src="js/basic/respond.min.js"></script>
    <script src="js/basic/html5shiv.min.js"></script>
    <![endif]-->
</head>
