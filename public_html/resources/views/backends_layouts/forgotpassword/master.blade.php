<!DOCTYPE html>
<html lang="en">
  
@include('backends_layouts.forgotpassword.head')
  <body class="login-bg login-container-bg">
         @yield('content')
   
    @include('backends_layouts.forgotpassword.footer')
  </body>
</html>