<!DOCTYPE html>
<html lang="en">
  <head>
    <title>DrawApp MANAGEMENT PORTAL - Forgot Password</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.ico')}}">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="{{ asset('assets/css/basic.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/general.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/theme.css')}}" class="style-theme">
    
    <!--[if lt IE 9]>
    <script src="js/basic/respond.min.js"></script>
    <script src="js/basic/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <style type="text/css">
    .reset-input{
        border: 1px solid #ccc;
        width: 50px;
        background: #FFF;
        border-color: #E2E2E2;
        box-shadow: none;
        border-radius: 7px;
        height: 40px;
        margin-right: 20px;
        text-align: center;
    }
    .modal-dialog {
      height: 80% !important;
      padding-top:6%;
    }
    /*.modal-content {
      height: 100% !important;
      overflow:visible;
    }*/
    .modal-body {
      height: 80%;
      overflow: auto;
    }
   
    /*progressbar*/
    #progressbar {
        margin-bottom: 30px;
        overflow: hidden;
        /*CSS counters to number the steps*/
        counter-reset: step;
    }

    #progressbar li {
        list-style-type: none;
        color: white;
        text-transform: uppercase;
        font-size: 9px;
        width: 33.33%;
        float: left;
        position: relative;
        letter-spacing: 1px;
        margin-top: 10px;
    }

    #progressbar li:before {
        content: counter(step);
        counter-increment: step;
        width: 24px;
        height: 24px;
        line-height: 26px;
        display: block;
        font-size: 12px;
        color: #333;
        background: white;
        border-radius: 25px;
        margin: 0 auto 10px auto;
    }

    /*progressbar connectors*/
    #progressbar li:after {
        content: '';
        width: 100%;
        height: 2px;
        background: white;
        position: absolute;
        left: -50%;
        top: 9px;
        z-index: -1; /*put it behind the numbers*/
    }


    /*marking active/completed steps green*/
    /*The number of the step and the connector before it = green*/
    #progressbar li.active:before, #progressbar li.active:after {
        background: #000;
        color: white;
    }

  </style>