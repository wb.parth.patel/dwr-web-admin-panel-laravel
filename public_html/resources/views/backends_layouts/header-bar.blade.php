 <!--HEADER-->
 <header class="l-header l-header-1 t-header-1">
                <div class="navbar navbar-ason">
                    <div class="container-fluid">
                         <div class="navbar-header">
                            <button type="button" data-toggle="collapse" data-target="#ason-navbar-collapse" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="dashboard.php" class="navbar-brand widget-logo"><span class="logo-default-header"><img src="{{ asset('assets/img/logo_dark.png') }}" height="40" alt="DWR"></span></a>
                        </div>
                        <div id="ason-navbar-collapse" class="collapse navbar-collapse">
                        <!-- <ul class="nav navbar-nav">
                            <li>
                                <div class="widget-message message-in-header dropdown dropdown-in-header"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-calendar-o"></i><span class="label label-danger">10</span></a>
                                    <ul role="menu" class="dropdown-menu">
                                        <li class="dropdown-menu-header">Pending Appointments<span class="label label-danger">10</span></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="widget-message message-in-header dropdown dropdown-in-header"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-user"></i><span class="label label-info">5</span></a>
                                    <ul role="menu" class="dropdown-menu">
                                        <li class="dropdown-menu-header">New Users<span class="label label-info">5</span></li>
                                    </ul>
                                </div> -->
                            </li>
                            <!-- <li>
                                <div class="widget-notification notification-in-header dropdown dropdown-in-header"><a href="calendar.php" class="dropdown-toggle"><i class="fa fa-calendar"></i></a>
                                </div>
                            </li>
                            <li>
                                <div class="widget-notification notification-in-header dropdown dropdown-in-header"><a href="add-appointment.php" class="dropdown-toggle"><i class="fa fa-plus">&nbsp;&nbsp; </i> Add New Appointment</a>
                                </div>
                            </li> 
                        </ul>-->
                        <div class="widget-page-summary">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <!-- Profile Widget-->
                                    <div class="widget-profile profile-in-header">
                                        <button type="button" data-toggle="dropdown" class="btn dropdown-toggle"><span class="name">Jhon David</span><img src="{{ asset('assets/img/profile/profile.jpg')}}"></button>
                                        <ul role="menu" class="dropdown-menu">
                                            <li><a href="{{ url('myprofile/index') }}"><i class="fa fa-user"></i>My Account</a></li>
                                            <li class="power"><a href="{{ url('login') }}"><i class="fa fa-power-off"></i>Logout</a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER & SIDEBAR INCLUDE -->
            <!--Left Sidebar Content-->
            <aside id="sb-left" class="l-sidebar l-sidebar-1 t-sidebar-1">
                <!--Switcher-->
                <!-- Profile in sidebar-->
                <div class="widget-profile-2 profile-2-in-side-2 t-profile-2-3">
                    <div class="profile-2-wrapper">
                        <!-- <div class="profile-2-social-stats">
                            <div class="l-span-xs-4">
                                <div class="profile-2-status-nr text-danger">527</div>Likes
                            </div>
                            <div class="l-span-xs-4">
                                <div class="profile-2-status-nr text-info">232</div>Comments
                            </div>
                            <div class="l-span-xs-4">
                                <div class="profile-2-status-nr text-success">15</div>Messages
                            </div>
                        </div> -->
                        <div class="profile-2-chart">
                            <div class="hide rickshaw-visitors"></div>
                            <div id="rickshawVisitors"></div>
                            <div id="rickshawVisitorsLegend" class="visitors_rickshaw_legend"></div>
                        </div>
                    </div>
                </div>
            </aside>