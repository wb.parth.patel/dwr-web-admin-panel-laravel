@extends('backends_layouts.forgotpassword.master')
@section('content')
<!-- Page Content -->
 <!--SECTION-->
 <section class="l-main-container">
      <!--Main Content-->
      <div style="margin-top:5%">
        <div class="login-container">
          <!--Logo-->
          <h1 class="login-logo">
            <img src="{{ asset('assets/img/logo.png')}}" height="200" alt="DwRAPP">
           </h1>
          <h3 class="text-center">WELCOME TO DwRAPP MANAGEMENT PORTAL</h3>
          <!--Form-->
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                    </div>
                @endif
          <form id="forgotForm" role="form" action="{{ route('password.email') }}" class="login-form">
                @csrf
            <div class="form-group">
              <input id="forgotEmail" type="email" name="forgotEmail" placeholder="Please enter your email address" class="form-control  @error('email') is-invalid @enderror" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                @enderror
            </div>
                <a href="#" class="btn btn-dark btn-block btn-login" data-toggle="modal" data-target="#myModal">Submit</a>
            <!-- <button type="submit" class="btn btn-block btn-dark btn-login">Send Link</button> -->
          </form>
          <br>
          <div>
              <h4 class="text-center"><a href="{{ url('login') }}">Take me to login page</a></h4>
          </div>
        </div>
      </div><br>
      <div class="container">
          <p class="text-right">Copyrights © DrawApp 2021</p>
      </div>
    </section>
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">RESET PASSWORD STEP 1</h4>
          </div>
          <div class="modal-body text-center">
            <h4>We have sent you One Time code on your following email address.</h4>
            <p>jacks@drwapp.com</p>
            <a href="#">change email address?</a><br>
            <ul id="progressbar">
                <li class="active"></li>
                <li ></li>
                <li ></li>
            </ul>

            <p>please type the varification code sent to your email address</p>
            <span style="color: red">Incorrct OTP</span><br>
            <form>
              <div class="form-group">
                <input type="password" class="reset-input" minlength="1" maxlength="1" name="otp1">
                <input type="password" class="reset-input" minlength="1" maxlength="1" name="otp1">
                <input type="password" class="reset-input" minlength="1" maxlength="1" name="otp1">
                <input type="password" class="reset-input" minlength="1" maxlength="1" name="otp1">
              </div>
              <a href="#" class="btn btn-dark btn-login" data-dismiss="modal" style="width: 180px;" data-toggle="modal" data-target="#myModal2">Verify</a>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="myModal2" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">RESET PASSWORD STEP 2</h4>
          </div>
          <div class="modal-body text-center">
            <!-- <h4>Lorem ipsum is a placeholder text commonly used to demonstrate the visual form.</h4> -->
            <ul id="progressbar">
                <li></li>
                <li class="active"></li>
                <li ></li>
            </ul>
            <form>
              <p class="text-left">please enter password</p>
              <div class="form-group">
                <input type="password" class="form-control" name="pass" placeholder="New password"><br>
                <input type="password" class="form-control" name="cpass" placeholder="Retype password">
              </div>
              <div class="text-left">
                <ul>
                  <li><i class="fa fa-circle" aria-hidden="true"></i> Use 6 - 64 character.</li>
                  <li><i class="fa fa-circle" aria-hidden="true"></i> Besides letters, include at least a number or symbol (!@#$%^-_+=).</li>
                  <li><i class="fa fa-circle" aria-hidden="true"></i> Password is case sensitive.</li>
                  <li><i class="fa fa-circle" aria-hidden="true"></i> Avoid using the same password for multiple sites.</li>
                </ul>
              </div><br>
              <!-- <button type="submit" class="btn btn-dark btn-login" style="width: 180px;">Reset Password</button> -->
              <a href="#" class="btn btn-dark btn-login" data-dismiss="modal" style="width: 180px;" data-toggle="modal" data-target="#myModal3">Reset Password</a>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="myModal3" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">RESET PASSWORD STEP 3</h4>
          </div>
          <div class="modal-body text-center">
            <ul id="progressbar">
                <li></li>
                <li></li>
                <li class="active"></li>
            </ul>
            <h4>Your Password reset sucessfully!</h4>
            <div class="text-center">
              <i class="fa fa-check-circle fa-5x" aria-hidden="true"></i>
              <br>
              <h4>Lorem ipsum is a placeholder text commonly used to demonstrate the visual form.</h4>
            </div>
            <br>            
            <a href="index.php" class="btn btn-dark btn-login" style="width: 180px;">Take me to login</a>
          </div>
        </div>
      </div>
    </div>
<!-- END Page Content -->
@endsection
