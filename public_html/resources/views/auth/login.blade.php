@extends('backends_layouts.login.master')
@section('content')
<!--Main Content-->
<div style="margin-top:2%">
          <!-- login-wrapper -->
        <div class="login-container">
          <!--Logo-->
          <a href="dashboard.php">
            <h6 class="login-logo">
              <img src="{{ asset('assets/img/logo.png')}}" height="210" alt="DrawApp">
            </h6>
          </a>
          <h3 class="text-center">WELCOME TO DwRAPP MANAGEMENT PORTAL</h3>
          <!--Login Form-->
          <form id="loginForm" action="{{ route('login') }} class="login-form">
              @csrf
            <div class="form-group">
              <input id="loginEmail" type="email" autocomplete="email" autofocus maxlength="40" name="loginEmail" value="{{ old('email') }}" placeholder="eMail Address" class="form-control @error('email') is-invalid @enderror" required>

               @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror 
            </div>
            <div class="form-group">
              <input id="loginPassword" type="password" maxlength="20" name="loginPassword" placeholder="Password" class="form-control @error('password') is-invalid @enderror" required autocomplete="current-password">
                @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                @enderror
            </div>
                
                <!-- <a href="dashboard.php" class="btn btn-dark btn-block btn-login">Login</a> -->
                <a href="{{url('dashboard')}}" class="btn btn-dark btn-block btn-login">Login</a>
                <!-- <div class="login-options">
                    <a href="forgot-password.php" class="fl">FORGOT PASSWORD ?</a>   
                </div> -->
                <div class="login-options">
                @if (Route::has('password.request'))
                    <!-- <a href="forgot-password.php" class="fl">FORGOT PASSWORD ?</a>  -->
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                    </a>  
                @endif
               </div>
          </form>
        </div>
      </div>
      <div class="container">
          <p class="text-right">Copyrights © DrawApp 2021</p>
      </div>
@endsection
